#include <SFML/Graphics.hpp>
#include "animation.hpp"
#define JOTARO "jotaro_spritesheet_correct.png"

using namespace sf;

int main()
{
    int sizX = 44;
    int ofsetX = 422;
    int ofsetY = 140;
    int sizY = 57;
    int frame = 8;
    float delay = 80;
    Event event;
    Texture texture;
    texture.loadFromFile(JOTARO);
    IntRect rectSourceSprite(ofsetX,ofsetY,sizX,sizY);
    Sprite sprite(texture, rectSourceSprite);
    sprite.setScale(2,2);
    sprite.setPosition(100,100);
    RenderWindow app(VideoMode(800, 600), "SFML window");
    app.setFramerateLimit(120);
    Clock horl;
    while (app.isOpen())
    {
        while (app.pollEvent(event))
        {
            if (event.type == Event::Closed)
                app.close();
        }
        animation(&rectSourceSprite, &horl, frame, sizX, ofsetX, delay);
        sprite.setTextureRect(rectSourceSprite);
        app.clear();
        app.draw(sprite);
        app.display();
    }
}
