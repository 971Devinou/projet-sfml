#include <SFML/Graphics.hpp>
#include <string.h>

using namespace sf;

typedef struct
{
    int offsetX;
    int offsetY;
    int sizeX;
    int sizeY;
    int maxFrames;
} AnimCoord;

typedef struct
{
    int id;
    char nom[];
    char nomStand[];
    int ptsVie = 250;
    int special = 100;
    int buff = 15;
    Texture persoTex;
    Sprite persoSprite;
} Personnage;

int prendDegats(int ptsVie, int degats);
Personnage creerPersonnage(int id, char nom[], char nomStand[], char spriteSheet[]);
void animations(Personnage* instance, AnimCoord anim, int* frame);
