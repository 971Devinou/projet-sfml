//Fait grace � un programme python
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string.h>
#include "personnage_type.hpp"
#define SCREEN_WIDTH 1300
#define SCREEN_LENGHT 700
#define FPS 120
#define JOTARO_SHEET "jotaro_spritesheet_correct.png"
using namespace sf;

int main()
{

    RenderWindow fen(VideoMode(SCREEN_WIDTH, SCREEN_LENGHT), "Jojo Star Fighter");
    fen.setFramerateLimit(FPS);
    Personnage jotaro;
    AnimCoord idleJotaro;
    idleJotaro.maxFrames = 5;
    jotaro = creerPersonnage(0, "Jotaro", "Star Platinium", JOTARO_SHEET);
    jotaro.persoSprite.setTexture(jotaro.persoTex);
    idleJotaro.offsetX = 6;
    idleJotaro.offsetY = 34;
    idleJotaro.sizeX = 37;
    idleJotaro.sizeY = 57;
    idleJotaro.maxFrames = 5;
    jotaro.persoSprite.setScale(2,2);

    while (fen.isOpen())
    {
        int frame = 0;
        Event event;
        while (fen.pollEvent(event))
        {
            if (event.type == Event::Closed)
                fen.close();


        }
        animations(&jotaro, idleJotaro, &frame);
        //jotaro.persoSprite.setTextureRect(IntRect(6, 34, 37, 57));
        fen.draw(jotaro.persoSprite);
        fen.display();

    }

    return EXIT_SUCCESS;
}

