#include "personnage_type.hpp"
#include <SFML/Graphics.hpp>
#include <string.h>

using namespace sf;

int prendDegats(int ptsVie, int degats)
{
    ptsVie = ptsVie - degats;
    return ptsVie;
}

Personnage creerPersonnage(int id, char nom[], char nomStand[], char spriteSheet[])
{
    Personnage instance;
    instance.id = id;
    strcpy(instance.nom, nom);
    strcpy(instance.nomStand, nomStand);
    instance.persoTex.loadFromFile(spriteSheet);
    return instance;
}

void animations(Personnage* instance, AnimCoord anim, int* frame)
{
    if (*frame > anim.maxFrames)
    {
        *frame = 0;
    }
    else
    {
      *frame++;
    }
    printf("%i\n", *frame);
    instance->persoSprite.setTextureRect(IntRect(anim.offsetX+(anim.offsetX*(*frame)), anim.offsetY, anim.sizeX, anim.sizeY));


}
