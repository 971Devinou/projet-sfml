#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <string.h>
#define JOTARO "jotaro_spritesheet_correct.png"
#define POLNAREFF "Polnaredd_fini.png"

using namespace sf;

typedef struct
{
    int offsetX;
    IntRect animCoord;
    int frameLimit;
    float delayMillis;
    //SoundBuffer animBuffer;
} Anim;

typedef struct
{
    bool attack = false;
    bool attackStand = false;
} AttackType;

typedef struct
{
    bool normal = false;
    bool special = false;
}Attack;

typedef struct
{
    char nom[20];
    int vie = 5000;
    int VIE_MAX = 5000;
    int degats = 0;
    bool attackInst = true;
    bool jump = false;
    int fighterAnim = 0;
    int standAnim = 0;
    Sprite fighter;
    bool externStand;
    Sprite stand;
    Clock fighterCl;
    Clock standCl;
    bool guardActive = 0;
    bool standActive = 0;
    Vector2f velocity = Vector2f(0.f,0.f);
    int standOffset = 0;
    Anim animation[15];
    Attack attackQualification;
    AttackType attackMode;
    bool controlAcces = true;

    //int fighterFrame = 0;
    //Sound fighterSound;
} Fighter;

void animation(Sprite *sprite, Anim *coord, Clock *clock);
void hit(Fighter *fighterOne, Fighter *fighterTwo);
bool isOnGround(Sprite object, RectangleShape ground);
void input(Event evenement, Fighter *player, RectangleShape sol ,Keyboard::Key moveRight, Keyboard::Key moveLeft, Keyboard::Key jump, Keyboard::Key guard, Keyboard::Key attack, Keyboard::Key specialAttack);
Anim setAnimation(IntRect coord, int frameLimit, float delayInterFrames);
