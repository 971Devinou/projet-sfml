#include <SFML/Graphics.hpp>
#include "animation.hpp"
#define PAS 4.f
#define PAS_WALKING 2.f
#define PAS_MINUS -4.f
#define PAS_WALKING_MINUS -2.f
#define STAND_OFFSET 67
#define STAND_OFFSET_MINUS -67
#define FRAMERATE 120
#define IDLE 0
#define MOVE 1
#define GUARD 2
#define STAND_GUARD 3
#define JUMP_START 4
#define IN_AIR 5
#define JUMP_END 6
#define NORMAL_ATTACK_ONE 7
#define NORMAL_ATTACK_TWO 8
#define UP_SPECIAL 9
#define SPECIAL 10
#define DOWN_SPECIAL 11
#define HIT 12
#define FALL 13

using namespace sf;

void animation(Sprite *sprite, Anim *coord, Clock *clock)
{
    if(clock->getElapsedTime().asMilliseconds()  > coord->delayMillis)
    {
        if(coord->animCoord.left > (coord->frameLimit-2)*(coord->animCoord.width)+coord->offsetX)
        {
            coord->animCoord.left = coord->offsetX;
        }

        else
            coord->animCoord.left += coord->animCoord.width;
        clock->restart();
    }
    sprite->setTextureRect(coord->animCoord);
    sprite->setOrigin(coord->animCoord.width/2, coord->animCoord.height);
}

void hit(Fighter *fighterOne, Fighter *fighterTwo)
{
    Fighter fighterOneTemp = *fighterOne;
    Fighter fighterTwoTemp = *fighterTwo;

    if (fighterOneTemp.attackMode.attack == true || fighterOneTemp.attackMode.attackStand == true || fighterTwoTemp.attackMode.attack == true || fighterTwoTemp.attackMode.attackStand == true)
    {
        if (fighterOneTemp.externStand == true)
        {
            if (fighterOneTemp.standActive == true && fighterOneTemp.attackQualification.special == true && fighterOneTemp.attackMode.attackStand == true)
            {
                if(fighterOneTemp.stand.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                {
                    fighterTwoTemp.vie-= 5;
                    fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                }
            }
            if (fighterOneTemp.standActive == false && fighterOneTemp.attackQualification.normal == true && fighterOneTemp.attackMode.attack == true)
            {
                if(fighterOneTemp.fighter.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                {
                    fighterTwoTemp.vie -= 10;
                    fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                }
            }
        }



        if (fighterTwoTemp.externStand == true)
        {
            if (fighterTwoTemp.standActive == true && fighterTwoTemp.attackQualification.special == true && fighterTwoTemp.attackMode.attackStand == true)
            {
                if(fighterTwoTemp.stand.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                {
                    fighterOneTemp.vie-= 5;
                    fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                }
            }
            if (fighterTwoTemp.standActive == false && fighterTwoTemp.attackQualification.normal == true && fighterTwoTemp.attackMode.attack == true)
            {
                if(fighterTwoTemp.fighter.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                {
                    fighterOneTemp.vie -= 10;
                    fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                }
            }
        }

    }
    *fighterOne = fighterOneTemp;
    *fighterTwo = fighterTwoTemp;
}

bool isOnGround(Sprite object, RectangleShape ground)
{
    bool results = false;
    if (object.getGlobalBounds().intersects(ground.getGlobalBounds()))
    {
        results = true;
    }
    return results;
}

void input(Event evenement, Fighter *player, RectangleShape sol,Keyboard::Key moveRight, Keyboard::Key moveLeft, Keyboard::Key jump, Keyboard::Key guard, Keyboard::Key attack, Keyboard::Key specialAttack)
{
    Fighter theFighter = *player;
    const float maxY = 50.f;
    const Vector2f gravity(0.f, 8.f);
    Vector2f velocity(2.f, 5.f);

    if (theFighter.controlAcces == true)
    {
        switch (evenement.type)
        {
        case Event::KeyPressed:
            if (evenement.key.code == moveRight)
            {

                theFighter.velocity.x = PAS;
                theFighter.fighterAnim = MOVE;
                theFighter.fighter.setScale(2,2);

                if (theFighter.externStand == true)
                {
                    theFighter.standOffset = STAND_OFFSET;
                    theFighter.stand.setScale(2,2);
                }
                theFighter.guardActive = false;

            }
            else if (evenement.key.code == moveLeft)
            {
                theFighter.velocity.x = PAS_MINUS;
                theFighter.fighter.setScale(-2,2);
                theFighter.fighterAnim = MOVE;
                theFighter.guardActive = false;
                if (theFighter.externStand == true)
                {
                    theFighter.stand.setScale(-2,2);
                    theFighter.standOffset = STAND_OFFSET_MINUS;
                }

            }
            else if (evenement.key.code == guard && theFighter.velocity.x == 0)
            {
                theFighter.fighterAnim = GUARD;
                theFighter.guardActive = true;
                if (theFighter.externStand == true)
                {
                    theFighter.standAnim = STAND_GUARD;
                    theFighter.standActive = false;
                }

            }
            else if (evenement.key.code == attack && theFighter.velocity.x == 0)
            {
                theFighter.guardActive = false;
                theFighter.attackQualification.normal = true;
                theFighter.attackMode.attack = true;
                if (theFighter.attackInst == false)
                {
                    theFighter.fighterAnim = NORMAL_ATTACK_ONE;
                    //theFighter.attackInst = !theFighter.attackInst;
                }
                else if (theFighter.attackInst == true)
                {
                    theFighter.fighterAnim = NORMAL_ATTACK_TWO;
                    //theFighter.attackInst = !theFighter.attackInst;
                }

            }
            else if (evenement.key.code == specialAttack && theFighter.velocity.x == 0)
            {
                //theFighter.fighterAnim = 0;
                theFighter.velocity.x = 0;
                theFighter.guardActive = false;
                theFighter.attackMode.attack = true;
                theFighter.attackQualification.special = true;

                if (theFighter.externStand == true)
                {
                    theFighter.fighterAnim = IDLE;
                    theFighter.standAnim = SPECIAL;
                    theFighter.standActive = true;
                    theFighter.attackMode.attackStand = true;
                }
                else if (theFighter.externStand == false)
                {
                    theFighter.fighterAnim = SPECIAL;
                    theFighter.attackMode.attack = true;
                }

            }
            if (evenement.key.code == jump && isOnGround(theFighter.fighter, sol))
            {
                theFighter.fighterAnim = JUMP_START;
                theFighter.jump = true;
                theFighter.velocity.y = -10.0f;
            }


            break;
        case Event::KeyReleased:
            if (evenement.key.code == moveRight || evenement.key.code == moveLeft)
            {
                theFighter.velocity.x = 0;
                theFighter.guardActive = false;

            }
            if (evenement.key.code == guard)
            {
                theFighter.guardActive = false;

                if (theFighter.externStand == true)
                    theFighter.standActive = false;
            }

            if (evenement.key.code == attack)
            {
                theFighter.guardActive = false;
                theFighter.attackMode.attack = false;
                theFighter.attackQualification.normal = false;
            }

            if (evenement.key.code == specialAttack)
            {
                theFighter.guardActive = false;
                theFighter.attackQualification.special = false;
                theFighter.attackMode.attack = false;
                if(theFighter.externStand == true)
                {
                    theFighter.standActive = false;
                    theFighter.attackMode.attackStand = false;
                }
            }
            if (evenement.key.code == jump)
            {
                theFighter.velocity.y = IDLE;
            }

            if (theFighter.velocity.x == 0)
                theFighter.fighterAnim = IDLE;

            break;
        }
    }
    else
    {
        printf("Acces a moi desactive\n");
    }

    *player = theFighter;
}

Anim setAnimation(IntRect coord, int frameLimit, float delayInterFrames)
{
    Anim results;
    results.animCoord = coord;
    results.delayMillis = delayInterFrames;
    results.frameLimit = frameLimit;
    results.offsetX = coord.left;
    return results;
}
