#include <SFML/Graphics.hpp>
#include"SelectionAreneLol.hpp"
#define NOM_ARENE1 "img/Arene1C.png"
#define NOM_ARENE2 "imgArene2/Arene2C.png"
#define NOM_ARENE3 "imgArene3/Arene3C.png"
#define NOM_ARENE4 "imgArene4/Arene4C.png"
#define NOM_ARENE5 "imgArene5/Arene5C.png"
#define PERSO_AVDOL "imgPerso/Avdol.jpg"
#define PERSO_DIO "imgPerso/Dio.png"
#define PERSO_HOLHORSE "imgPerso/HolHorse.jpg"
#define PERSO_JOSEPH "imgPerso/Joseph.jpg"
#define PERSO_JOSUKE "imgPerso/Josuke.png"
#define PERSO_JOTARO "imgPerso/Jotaro.png"
#define PERSO_KAKYOIN "imgPerso/Kakyoin.jfif"
#define PERSO_POLNAREFF "imgPerso/Polnareff.PNG"

using namespace sf;

int main()
{


//void choixArene(int  valeurChoixArene, int choixJoueur1, int choixJoueur2)

    RenderWindow app(VideoMode(1300, 700), "SFML window");
    int valeurChoixArene=5;
    int choixJoueur1=4;
    int choixJoueur2=6;

//////////////////////////////////////////////////

    Sprite Avdol;
    Sprite Dio;
    Sprite HolHorse;
    Sprite Joseph;
    Sprite Josuke;
    Sprite Jotaro;
    Sprite Kakyoin;
    Sprite Polnareff;

    Texture imageAvdol;
    Texture imageDio;
    Texture imageHolHorse;
    Texture imageJoseph;
    Texture imageJosuke;
    Texture imageJotaro;
    Texture imageKakyoin;
    Texture imagePolnareff;


    Sprite Personnage [8];
    Personnage[0]=Avdol;
    Personnage[1]=Dio;
    Personnage[2]= HolHorse;
    Personnage[3]= Joseph;
    Personnage[4]=Josuke;
    Personnage[5]= Jotaro;
    Personnage[6]= Kakyoin;
    Personnage[7]= Polnareff;

    Texture Perso[8];
    Perso[0]= imageAvdol;
    Perso[1]= imageDio;
    Perso[2]=imageHolHorse;
    Perso[3]=imageJoseph;
    Perso[4]=imageJosuke;
    Perso[5]=imageJotaro;
    Perso[6]=imageKakyoin;
    Perso[7]=imagePolnareff;

    Perso[0].loadFromFile(PERSO_AVDOL);
    Perso[1].loadFromFile(PERSO_DIO);
    Perso[2].loadFromFile(PERSO_HOLHORSE);
    Perso[3].loadFromFile(PERSO_JOSEPH);
    Perso[4].loadFromFile(PERSO_JOSUKE);
    Perso[5].loadFromFile(PERSO_JOTARO);
    Perso[6].loadFromFile(PERSO_KAKYOIN);
    Perso[7].loadFromFile(PERSO_POLNAREFF);

    Personnage[0].setTexture(Perso[0]);
    Personnage[1].setTexture(Perso[1]);
    Personnage[2].setTexture(Perso[2]);
    Personnage[3].setTexture(Perso[3]);
    Personnage[4].setTexture(Perso[4]);
    Personnage[5].setTexture(Perso[5]);
    Personnage[6].setTexture(Perso[6]);
    Personnage[7].setTexture(Perso[7]);



//////////////////////////////////////////////////


    Sprite Arene1;
    Texture imageArene1;
    imageArene1.loadFromFile(NOM_ARENE1);


    Sprite Arene2;
    Texture imageArene2;
    imageArene2.loadFromFile(NOM_ARENE2);

    Sprite Arene3;
    Texture imageArene3;
    imageArene3.loadFromFile(NOM_ARENE3);

    Sprite Arene4;
    Texture imageArene4;
    imageArene4.loadFromFile(NOM_ARENE4);

    Sprite Arene5;
    Texture imageArene5;
    imageArene5.loadFromFile(NOM_ARENE5);

    RectangleShape barre2(Vector2f(250,10));
    barre2.setPosition(1053,100);
    barre2.setFillColor(Color :: Green);

    RectangleShape barre(Vector2f(250,10));
    barre.setPosition(0,100);
    barre.setFillColor(Color :: Green);



/////////////////////////////////////////////////////////

    if (choixJoueur1==0)
        {
            Personnage[0].setPosition(0,0);
            Personnage[0].setScale(0.38,0.26);
        }
    else if (choixJoueur2==0)
        {
            Personnage[0].setPosition(1150,0);
            Personnage[0].setScale(0.38,0.26);
        }


//////////////////////////////////////////////////


    if (choixJoueur1==1)
        {
            Personnage[1].setPosition(0,0);
            Personnage[1].setScale(0.7,0.45);
        }
    else if (choixJoueur2==1)
        {
            Personnage[1].setPosition(1150,0);
            Personnage[1].setScale(0.7,0.45);
        }


///////////////////////////////////////////////////


    if (choixJoueur1==2)
        {
            Personnage[2].setPosition(0,0);
            Personnage[2].setScale(0.68,0.287);
        }
    else if (choixJoueur2==2)
        {
            Personnage[2].setPosition(1150,0);
            Personnage[2].setScale(0.68,0.287);
        }

 //////////////////////////////////////////////////////


    if (choixJoueur1==3)
        {
            Personnage[3].setPosition(0,0);
            Personnage[3].setScale(0.4,0.26);
        }
    else if (choixJoueur2==3)
        {
            Personnage[3].setPosition(1150,0);
            Personnage[3].setScale(0.4,0.26);
        }

 /////////////////////////////////////////////////////////


    if (choixJoueur1==4)
        {
            Personnage[4].setPosition(0,0);
            Personnage[4].setScale(0.9,0.44);
        }
    else if (choixJoueur2==4)
        {
            Personnage[4].setPosition(1157,0);
            Personnage[4].setScale(0.92,0.44);
        }


//////////////////////////////////////////////////////////


    if (choixJoueur1==5)
        {
            Personnage[5].setPosition(0,0);
            Personnage[5].setScale(0.4,0.26);
        }
    else if (choixJoueur2==5)
        {
            Personnage[5].setPosition(1150,0);
            Personnage[5].setScale(0.4,0.26);
        }

 ///////////////////////////////////////////////////////////

    if (choixJoueur1==6)
        {
            Personnage[6].setPosition(0,0);
            Personnage[6].setScale(0.7,0.445);
        }
    else if (choixJoueur2==6)
        {
            Personnage[6].setPosition(1150,0);
            Personnage[6].setScale(0.72,0.44);
        }

 ///////////////////////////////////////////////////////////

    if (choixJoueur1==7)
        {
            Personnage[7].setPosition(0,0);
            Personnage[7].setScale(0.35,0.21);
        }
    else if (choixJoueur2==7)
        {
            Personnage[7].setPosition(1150,0);
            Personnage[7].setScale(0.36,0.21);
        }

    int i = 0;
    Clock montre;


    while(app.isOpen())
    {
        Event event;


        while (app.pollEvent(event))
        {

            if (event.type == Event::Closed)
                app.close();
        }


        if(valeurChoixArene==1)

        {
            Arene1.setTexture(imageArene1);
            Arene1.setScale(2,2.1);
            if (i > 7)
            {
                i = 0;
            }
            else
            {
                if (montre.getElapsedTime().asMilliseconds() > 85.0f)
                {
                    Arene1.setTextureRect(IntRect(120 + 800*i,0,800,336));
                    i += 1;
                    montre.restart();
                }

            }

            app.clear();
            app.draw(Arene1);
            app.draw(barre);
            app.draw(barre2);
            app.draw(Personnage[choixJoueur1]);
            app.draw(Personnage[choixJoueur2]);


            app.display();


        }

        if (valeurChoixArene==2)
        {
            Arene2.setTexture(imageArene2);
            Arene2.setScale((2.5),(1.85));

            if (i > 7)
            {
                i = 0;
            }
            else
            {
                if (montre.getElapsedTime().asMilliseconds() > 85.0f)
                {
                    Arene2.setTextureRect(IntRect(120 + 768*i,0,768,384));
                    i += 1;
                    montre.restart();
                }

            }

            app.clear();
            app.draw(Arene2);
            app.draw(barre);
            app.draw(barre2);
            app.draw(Personnage[choixJoueur1]);
            app.draw(Personnage[choixJoueur2]);
            app.display();
        }

        if(valeurChoixArene==3)
        {
            Arene3.setTexture(imageArene3);
            Arene3.setScale((2.5),(3.12));

            if (i > 7)
            {
                i = 0;
            }
            else
            {
                if (montre.getElapsedTime().asMilliseconds() > 85.0f)
                {
                    Arene3.setTextureRect(IntRect(120 + 752*i,0,752,224));
                    i += 1;
                    montre.restart();
                }

            }

            app.clear();
            app.draw(Arene3);
            app.draw(barre);
            app.draw(barre2);
            app.draw(Personnage[choixJoueur1]);
            app.draw(Personnage[choixJoueur2]);
            app.display();
        }

        if(valeurChoixArene==4)
        {
            Arene4.setTexture(imageArene4);
            Arene4.setScale((2.05),(2.75));

            if (i > 7)
            {
                i = 0;
            }
            else
            {
                if (montre.getElapsedTime().asMilliseconds() > 85.0f)
                {
                    Arene4.setTextureRect(IntRect(120 + 768*i,0,768,256));
                    i += 1;
                    montre.restart();
                }

            }

            app.clear();
            app.draw(Arene4);
            app.draw(barre);
            app.draw(barre2);
            app.draw(Personnage[choixJoueur1]);
            app.draw(Personnage[choixJoueur2]);
            app.display();
        }

        if(valeurChoixArene==5)
        {
            Arene5.setTexture(imageArene5);

            Arene5.setScale((2.1),(3.15));

            if (i > 7)
            {
                i = 0;
            }
            else
            {
                if (montre.getElapsedTime().asMilliseconds() > 85.0f)
                {
                    Arene5.setTextureRect(IntRect(120 + 752*i,0,752,224));
                    i += 1;
                    montre.restart();
                }

            }

            app.clear();
            app.draw(Arene5);
            app.draw(barre);
            app.draw(barre2);
            app.draw(Personnage[choixJoueur1]);
            app.draw(Personnage[choixJoueur2]);
            app.display();

        }

    }

}
