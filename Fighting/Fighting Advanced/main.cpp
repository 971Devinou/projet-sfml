#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include "animation.hpp"
#define ORAORA "ORA_ORA.wav"
#define FRAMERATE 120
#define PAS 4.f
#define PAS_WALKING 2.f
#define PAS_MINUS -4.f
#define PAS_WALKING_MINUS -2.f
#define STAND_OFFSET 67
#define STAND_OFFSET_MINUS -67
#define IDLE 0
#define MOVE 1
#define GUARD 2
#define STAND_GUARD 3
#define JUMP_START 4
#define IN_AIR 5
#define JUMP_END 6
#define NORMAL_ATTACK_ONE 7
#define NORMAL_ATTACK_TWO 8
#define UP_SPECIAL 9
#define SPECIAL 10
#define DOWN_SPECIAL 11
#define HIT 12
#define FALL 13
#define BATTU 14

using namespace sf;

int main()
{

    Fighter Jotaro;
    Texture fighterJotaro;
    Fighter Polnareff;
    Texture fighterPolnareff;
    Polnareff.externStand = false;
    Jotaro.externStand = true;
    const float maxY = 50.f;
    const Vector2f gravity(0.f, 8.f);
    Vector2f velocity(2.f, 5.f);

    RenderWindow app(VideoMode(1300, 700), "Fighting");
    app.setFramerateLimit(FRAMERATE);

    Jotaro.animation[IDLE] = setAnimation(IntRect(6,34,37,57), 6, 180); //Idle
    Jotaro.animation[MOVE] = setAnimation(IntRect(647,34,46,57), 8, 70); //Run
    Jotaro.animation[GUARD] = setAnimation(IntRect(201,141,35,56), 1, 600); //Guard
    Jotaro.animation[STAND_GUARD] = setAnimation(IntRect(284,135,38,62), 2, 600); //Stand Guard
    Jotaro.animation[JUMP_START] = setAnimation(IntRect(422,140,44,57), 2, 50.0);// Jump start
    Jotaro.animation[IN_AIR] = setAnimation(IntRect(466, 140, 44, 57), 2, 100); //In air
    Jotaro.animation[JUMP_END] = setAnimation(IntRect(598,140,44,57), 4, 50); //Jump end
    Jotaro.animation[SPECIAL] = setAnimation(IntRect(208,936,101,66), 6, 80); //Special
    Jotaro.animation[FALL] = setAnimation(IntRect(2,233,63,55), 7, 100);
    Jotaro.animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(1,711,53,59), 4, 80);

    Polnareff.animation[IDLE] = setAnimation(IntRect(0,0,33,59), 4, 250); //Idle
    Polnareff.animation[MOVE] = setAnimation(IntRect(0,181,52,55), 8, 80); //Move
    Polnareff.animation[GUARD] = setAnimation(IntRect(0,120,59,61), 2, 600); //Guard
    Polnareff.animation[JUMP_START] = setAnimation(IntRect(0,59,48,61), 2, 100);
    Polnareff.animation[IN_AIR] = setAnimation(IntRect(48,59,48,61), 2, 100);
    Polnareff.animation[JUMP_END] = setAnimation(IntRect(144,59,48,61), 3, 100);
    Polnareff.animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(0,530,61,61), 4, 100);
    Polnareff.animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(0,591,67,57), 5, 100);
    Polnareff.animation[UP_SPECIAL] = setAnimation(IntRect(105,428,105,102), 2, 100);
    Polnareff.animation[SPECIAL] = setAnimation(IntRect(0,356,106,72), 3, 100);
    Polnareff.animation[DOWN_SPECIAL] = setAnimation(IntRect(0,648, 87,72), 4, 100);
    Polnareff.animation[HIT] = setAnimation(IntRect(0,298,46,58), 2, 80);
    Polnareff.animation[FALL] = setAnimation(IntRect(0,242,71,56), 3, 100);
    Polnareff.animation[BATTU] = setAnimation(IntRect(142,242,71,56), 1, 100);

    fighterJotaro.loadFromFile(JOTARO);
    Jotaro.fighter.setTexture(fighterJotaro);
    Jotaro.fighter.setTextureRect(IntRect(6, 34, 37, 57));
    //Jotaro.fighter.setOrigin(37/2, 57/2);
    Jotaro.fighter.setScale(2,2);
    Jotaro.fighter.setPosition(100,599);
    Jotaro.stand.setTexture(fighterJotaro);
    Jotaro.stand.setOrigin(0, 62);
    Jotaro.stand.setScale(2,2);

    fighterPolnareff.loadFromFile(POLNAREFF);
    Polnareff.fighter.setTexture(fighterPolnareff);
    Polnareff.fighter.setTextureRect(IntRect(6, 34, 37, 57));
    //Polnareff.fighter.setOrigin(37/2, 57/2);
    Polnareff.fighter.setScale(-2,2);
    Polnareff.fighter.setPosition(1000,599);
    Polnareff.stand.setTexture(fighterPolnareff);
    //Polnareff.stand.setOrigin(38, 62);
    Polnareff.stand.setScale(2,2);

    RectangleShape sol(Vector2f(1300, 15));
    sol.setPosition(0, 600);
    sol.setFillColor(Color::Yellow);

    Polnareff.controlAcces = true;
    Jotaro.controlAcces = true;
    Polnareff.attackQualification.attackBurst = false;
    Jotaro.controlAcces = true;
    Jotaro.attackQualification.attackBurst = true;
    Jotaro.attackQualification.burstCount = 10;

    Polnareff.canAnim = true;
    Jotaro.canAnim = true;

    while (app.isOpen())
    {

        Event event;

        if (app.pollEvent(event))
        {


        }
        if (Event::Closed)
                app.close();


        input(&Jotaro, sol, Keyboard::D, Keyboard::Q, Keyboard::Z, Keyboard::S, Keyboard::R, Keyboard::T);
        input(&Polnareff, sol, Keyboard::Right, Keyboard::Left, Keyboard::Up, Keyboard::Down, Keyboard::Numpad0, Keyboard::Numpad2);


        if (!isOnGround(Jotaro.fighter, sol))
        {
            if(Jotaro.jump)
                trueAnimation(&Jotaro, JUMP_START, 1);
            Jotaro.fighterAnim = IN_AIR;
        }
        else
        {
            if (Jotaro.velocity.x == 0 && (Jotaro.attackMode.attack == false && Jotaro.attackMode.attackStand == false))
            {
                Jotaro.fighterAnim = IDLE;
            }
        }
        if (!isOnGround(Polnareff.fighter, sol))
			{
				Polnareff.fighterAnim = IN_AIR;
			}
        else
        {
            if (Polnareff.velocity.x == 0 && (Polnareff.attackMode.attack == false && Polnareff.attackMode.attackStand == false))
            {
                Polnareff.fighterAnim = IDLE;
            }

        }


        //Jotaro.fighter.move(Jotaro.velocity);
        //Jotaro.stand.setPosition(Jotaro.fighter.getPosition().x+Jotaro.standOffset, Jotaro.fighter.getPosition().y); // On ajoute la taille de Jotaro
        //Polnareff.fighter.move(Polnareff.velocity);


        if (Polnareff.vie > 0 )
        {
            animation(&Polnareff.fighter, &Polnareff.animation[Polnareff.fighterAnim], &Polnareff.fighterCl);
        }
        else
        {

            //animation(&Polnareff.fighter, &Polnareff.animation[BATTU], &Polnareff.fighterCl);
            Polnareff.controlAcces = false;
            Polnareff.isAlive = false;
            trueAnimation(&Polnareff, FALL, 1);
            //system("PAUSE");
        }
        if (Jotaro.vie > 0 )
        {
            animation(&Jotaro.fighter, &Jotaro.animation[Jotaro.fighterAnim], &Jotaro.fighterCl);
            animation(&Jotaro.stand, &Jotaro.animation[Jotaro.standAnim], &Jotaro.standCl);
        }
        else
        {

            //animation(&Polnareff.fighter, &Polnareff.animation[BATTU], &Polnareff.fighterCl);
            Jotaro.controlAcces = false;
            Jotaro.isAlive = false;
            trueAnimation(&Jotaro, FALL, 1);
            //system("PAUSE");
        }

        hit(&Jotaro, &Polnareff);
        printf("|||||||||||%i %i", Polnareff.canAnim, Polnareff.animBegin);


        app.clear();
        app.draw(sol);
        app.draw(Jotaro.fighter);
        app.draw(Polnareff.fighter);

        if ((Jotaro.guardActive == 1 || Jotaro.standActive == 1) && Jotaro.externStand == true)
        {
            app.draw(Jotaro.stand);
        }
            app.display();


        }
    return EXIT_SUCCESS;
    }
