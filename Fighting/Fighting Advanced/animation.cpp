#include <SFML/Graphics.hpp>
#include "animation.hpp"
#define PAS 4
#define PAS_WALKING 2
#define PAS_MINUS -4
#define PAS_WALKING_MINUS -2
#define STAND_OFFSET 67
#define STAND_OFFSET_MINUS -67
#define FRAMERATE 120
#define IDLE 0
#define MOVE 1
#define GUARD 2
#define STAND_GUARD 3
#define JUMP_START 4
#define IN_AIR 5
#define JUMP_END 6
#define NORMAL_ATTACK_ONE 7
#define NORMAL_ATTACK_TWO 8
#define UP_SPECIAL 9
#define SPECIAL 10
#define DOWN_SPECIAL 11
#define HIT 12
#define FALL 13
#define DEGATS_NORMAL 1
#define DEGATS_SUPER 10
#define SCALE 2

using namespace sf;

void animation(Sprite *sprite, Anim *coord, Clock *clock)
{
    if(clock->getElapsedTime().asMilliseconds()  > coord-> delayMillis)
    {
        if(coord->animCoord.left > (coord->frameLimit-2)*(coord->animCoord.width)+coord->offsetX)
        {
            coord->animCoord.left = coord->offsetX;
        }

        else
            coord->animCoord.left += coord->animCoord.width;
        clock->restart();
    }
    sprite->setTextureRect(coord->animCoord);
    sprite->setOrigin(coord->animCoord.width/2, coord->animCoord.height);
}

void trueAnimation(Fighter *fighterPointer, int animation, int nbAnim)
{
    Fighter theFighter = *fighterPointer;
    if(theFighter.canAnim)
    {
        //system("PAUSE");
        if(theFighter.animBegin)
        {
            theFighter.trueAnimCl.restart();
            theFighter.fighterCl.restart();
            //theFighter.controlAcces = false;
            theFighter.animBegin = false;
        }

        float dureeAnim;
        dureeAnim = theFighter.animation[animation].delayMillis*theFighter.animation[animation].frameLimit;

        if (theFighter.trueAnimCl.getElapsedTime().asMilliseconds() < dureeAnim*nbAnim)
        {
            if(theFighter.fighterCl.getElapsedTime().asMilliseconds()  > theFighter.animation[animation].delayMillis)
            {
                if(theFighter.animation[animation].animCoord.left > (theFighter.animation[animation].frameLimit-2)*(theFighter.animation[animation].animCoord.width)+theFighter.animation[animation].offsetX)
                {
                    theFighter.animation[animation].animCoord.left = theFighter.animation[animation].offsetX;
                }

                else
                    theFighter.animation[animation].animCoord.left += theFighter.animation[animation].animCoord.width;

                theFighter.fighterCl.restart();
            }
            if (theFighter.attackQualification.attackBurst == true && theFighter.attackQualification.special == true)
            {
                theFighter.stand.setTextureRect(theFighter.animation[animation].animCoord);
                theFighter.fighter.setOrigin(theFighter.animation[animation].animCoord.width/2, theFighter.animation[animation].animCoord.height);
            }
            else
            {
                theFighter.fighter.setTextureRect(theFighter.animation[animation].animCoord);
                theFighter.fighter.setOrigin(theFighter.animation[animation].animCoord.width/2, theFighter.animation[animation].animCoord.height);
            }

        }
        else
        {
            theFighter.canAnim = false;
            theFighter.animBegin = true;
            //theFighter.controlAcces = true;
        }
    }
    *fighterPointer = theFighter;
}

void hit(Fighter *fighterOne, Fighter *fighterTwo)
{
    Fighter fighterOneTemp = *fighterOne;
    Fighter fighterTwoTemp = *fighterTwo;
    int degatsOne = 0, degatsTwo = 0;

    if(fighterOneTemp.isAlive && fighterTwoTemp.isAlive)
    {
        if (fighterOneTemp.attackMode.attack == true || fighterOneTemp.attackMode.attackStand == true || fighterTwoTemp.attackMode.attack == true || fighterTwoTemp.attackMode.attackStand == true)
        {

            if (fighterOneTemp.externStand == true) //Si le joueur 1 (stand d�tach�) frappe le joueur 2 avec une attaque sp�ciale
            {
                if (fighterOneTemp.standActive == true && fighterOneTemp.attackQualification.special == true && fighterOneTemp.attackMode.attackStand == true)
                {
                    if(fighterOneTemp.stand.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                    {
                        degatsTwo = DEGATS_SUPER;
                        fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                    }
                }
            }

            if (fighterTwoTemp.externStand == true) //Si le joueur 2 (stand d�tach�) frappe le joueur 1 avec une attaque sp�ciale
            {
                if (fighterTwoTemp.standActive == true && fighterTwoTemp.attackQualification.special == true && fighterTwoTemp.attackMode.attackStand == true)
                {
                    if(fighterTwoTemp.stand.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                    {
                        degatsOne = DEGATS_SUPER;
                        fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                    }
                }
            }

            if (fighterOneTemp.externStand == false) // Attaques sp�ciales stand attach�
            {
                if (fighterOneTemp.attackQualification.special == true && fighterOneTemp.attackMode.attack == true)
                {
                    if(fighterOneTemp.fighter.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                    {
                        degatsTwo = DEGATS_SUPER;
                        fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                    }
                }
            }

            if (fighterTwoTemp.externStand == false) // Attaques sp�ciales stand attach�
            {
                if (fighterTwoTemp.attackQualification.special == true && fighterTwoTemp.attackMode.attack == true)
                {
                    if(fighterTwoTemp.fighter.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                    {
                        degatsOne = DEGATS_SUPER;
                        fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                    }
                }
            }
            if (fighterOneTemp.standActive == false && fighterOneTemp.attackQualification.normal == true && fighterOneTemp.attackMode.attack == true)
            {
                if(fighterOneTemp.fighter.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                {
                    degatsTwo = DEGATS_NORMAL;
                    fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                }
            }
            if (fighterTwoTemp.standActive == false && fighterTwoTemp.attackQualification.normal == true && fighterTwoTemp.attackMode.attack == true)
            {
                if(fighterTwoTemp.fighter.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                {
                    degatsOne = DEGATS_NORMAL;
                    fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                }
            }

        }
        if (fighterOneTemp.guardActive == true)
            degatsOne = degatsOne/2;
        if (fighterTwoTemp.guardActive == true)
            degatsTwo = degatsTwo/2;
    }
    else
    {
        degatsOne = 0;
        degatsTwo = 0;
    }


    fighterOneTemp.vie -= degatsOne;
    fighterTwoTemp.vie -= degatsTwo;
    *fighterOne = fighterOneTemp;
    *fighterTwo = fighterTwoTemp;
}

bool isOnGround(Sprite object, RectangleShape ground)
{
    bool results = false;
    if (object.getGlobalBounds().intersects(ground.getGlobalBounds()))
    {
        results = true;
    }
    return results;
}

void input(Fighter *player, RectangleShape sol,Keyboard::Key moveRight, Keyboard::Key moveLeft, Keyboard::Key jump, Keyboard::Key guard, Keyboard::Key attack, Keyboard::Key specialAttack)
{
    Fighter theFighter = *player;
    const float maxY = 50.f;
    const Vector2f gravity(0.f, 8.f);
    Vector2f velocity(2.f, 5.f);

    if (theFighter.controlAcces == true)
    {
        if (Keyboard::isKeyPressed(moveRight))
        {
            theFighter.velocity.x = PAS;
            theFighter.fighterAnim = MOVE;
            theFighter.fighter.setScale(SCALE,SCALE);
            if (theFighter.externStand == true)
            {
                theFighter.stand.setScale(SCALE,SCALE);
                //theFighter.standOffset = STAND_OFFSET_MINUS;
            }
            theFighter.guardActive = false;
            theFighter.attackQualification.special = false;
            theFighter.attackMode.attack = false;
            if(theFighter.externStand == true)
            {
                theFighter.standActive = false;
                theFighter.attackMode.attackStand = false;
            }
        }
        else if (!Keyboard::isKeyPressed(moveRight))
        {
            theFighter.velocity.x = 0;
        }
        if (Keyboard::isKeyPressed(moveLeft))
        {
            theFighter.velocity.x = - PAS;
            theFighter.fighterAnim = MOVE;
            theFighter.fighter.setScale(-SCALE,SCALE);
            if (theFighter.externStand == true)
            {
                theFighter.stand.setScale(-SCALE,SCALE);
                //theFighter.standOffset = STAND_OFFSET_MINUS;
            }
            theFighter.guardActive = false;
            theFighter.attackQualification.special = false;
            theFighter.attackMode.attack = false;
            if(theFighter.externStand == true)
            {
                theFighter.standActive = false;
                theFighter.attackMode.attackStand = false;
            }
        }
        if (Keyboard::isKeyPressed(guard))
        {

            theFighter.fighterAnim = GUARD;
            theFighter.guardActive = true;

            if (theFighter.externStand == true)
            {
                theFighter.standAnim = STAND_GUARD;
                theFighter.standActive = false;
            }
        }
        else if (!Keyboard::isKeyPressed(guard))
        {
            theFighter.guardActive = false;

            if (theFighter.externStand == true)
                theFighter.standActive = false;
        }
        if (Keyboard::isKeyPressed(attack) && theFighter.velocity.x == 0 && theFighter.velocity.y == 0)
        {
            theFighter.guardActive = false;
            theFighter.attackQualification.normal = true;
            theFighter.attackMode.attack = true;
            theFighter.fighterAnim = NORMAL_ATTACK_TWO;
        }
        else if (!Keyboard::isKeyPressed(attack) || theFighter.velocity.x != 0 || theFighter.velocity.y != 0)
        {
            theFighter.guardActive = false;
            theFighter.attackMode.attack = false;
            theFighter.attackQualification.normal = false;
        }
        if (Keyboard::isKeyPressed(specialAttack) && theFighter.velocity.x == 0 && theFighter.velocity.y == 0)
        {
            theFighter.velocity.x = 0;
                theFighter.guardActive = false;
                theFighter.attackQualification.special = true;

                if (theFighter.externStand == true)
                {
                    theFighter.fighterAnim = IDLE;
                    theFighter.standAnim = SPECIAL;
                    theFighter.standActive = true;
                    theFighter.attackMode.attackStand = true;
                }
                else if (theFighter.externStand == false)
                {
                    theFighter.fighterAnim = SPECIAL;
                    theFighter.attackMode.attack = true;
                }
        }
        else if (!Keyboard::isKeyPressed(specialAttack) || theFighter.velocity.x != 0 || theFighter.velocity.y != 0)
        {
                theFighter.attackQualification.special = false;
                if(theFighter.externStand == true)
                {
                    theFighter.standActive = false;
                    theFighter.attackMode.attackStand = false;
                }
        }
        if (Keyboard::isKeyPressed(jump))
            {
                theFighter.fighter.move(0, theFighter.pasSaut);
                if(theFighter.pasSaut<VITESSE_SAUT_MAX)
                    theFighter.pasSaut += GRAVITY;
            }
    }
    else
    {
        printf("Acces a moi desactive\n");
        theFighter.velocity.x = 0;
    }

    theFighter.fighter.move(theFighter.velocity);

    if (theFighter.externStand == true)
    {
        theFighter.standOffset = Vector2f(theFighter.animation[theFighter.fighterAnim].animCoord.width/2, theFighter.animation[theFighter.fighterAnim].animCoord.height/2);
        theFighter.stand.setPosition(theFighter.fighter.getPosition().x+(theFighter.standOffset.x*2)*theFighter.fighter.getScale().x, theFighter.fighter.getPosition().y);
    }

    if(theFighter.fighter.getPosition().y >= sol.getPosition().y)
        theFighter.pasSaut = SAUT_MAX;
    if(theFighter.fighter.getPosition().y < sol.getPosition().y)
    {
        theFighter.fighter.move(0, theFighter.pasSaut);
        if(theFighter.pasSaut<VITESSE_SAUT_MAX)
            theFighter.pasSaut += GRAVITY;
    }

    printf("SECOND VELOCITY : %i\n", theFighter.velocity.x);
    *player = theFighter;
}

Anim setAnimation(IntRect coord, int frameLimit, float delayInterFrames)
{
    Anim results;
    results.animCoord = coord;
    results.delayMillis = delayInterFrames;
    results.frameLimit = frameLimit;
    results.offsetX = coord.left;
    return results;
}
