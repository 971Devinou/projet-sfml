#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include "animation.hpp"
#define ORAORA "ORA_ORA.wav"
#define FRAMERATE 120
#define PAS 4.f
#define PAS_WALKING 2.f
#define PAS_MINUS -4.f
#define PAS_WALKING_MINUS -2.f
#define STAND_OFFSET 67
#define STAND_OFFSET_MINUS -67

using namespace sf;

int main()
{

    int frameJ = 0 ,frameD = 0;
    int animDav = 0;
    int standOffset = STAND_OFFSET;
    Fighter Jotaro;
    const float maxY = 50.f;
    const Vector2f gravity(0.f, 8.f);
    Vector2f velocity(2.f, 5.f);
    Vector2f velocityDavid(0.f,0.f);

    RenderWindow app(VideoMode(1300, 700), "Fighting");
    app.setFramerateLimit(FRAMERATE);
    Clock montreD, deltaTime;

    Jotaro.animation[0] = setAnimation(IntRect(6,34,37,57), 6, 100.f); //Idle
    Jotaro.animation[1] = setAnimation(IntRect(647,34,46,57), 8, 70.0f); //Run
    Jotaro.animation[2].offsetX = 166;// Guard
    Jotaro.animation[2].animCoord=IntRect(166,141,35,56);
    Jotaro.animation[2].frameLimit = 3;
    Jotaro.animation[2].delayMillis = 80.f;
    Jotaro.animation[3].offsetX = 284;//stand guard
    Jotaro.animation[3].animCoord=IntRect(284,135,38,62);
    Jotaro.animation[3].frameLimit = 2;
    Jotaro.animation[3].delayMillis = 600.f;
    Jotaro.animation[4].offsetX = 208; // Ora ora
    Jotaro.animation[4].frameLimit = 6;
    Jotaro.animation[4].delayMillis = 70.0f;
    Jotaro.animation[4].animCoord=IntRect(208,936,101,66);
    Jotaro.animation[5].offsetX = 422; // Jump start
    Jotaro.animation[5].frameLimit = 2;
    Jotaro.animation[5].delayMillis = 50.0f;
    Jotaro.animation[5].animCoord=IntRect(422,140,44,57);
    Jotaro.animation[6].frameLimit = 2; // In air
    Jotaro.animation[6].delayMillis = 100.f;
    Jotaro.animation[6].offsetX = 466;
    Jotaro.animation[6].animCoord = IntRect(466, 140, 44, 57);
    Jotaro.animation[7].offsetX = 598; // Jump end
    Jotaro.animation[7].frameLimit = 4;
    Jotaro.animation[7].delayMillis = 50.0f;
    Jotaro.animation[7].animCoord=IntRect(598,140,44,57);

    Texture davidT;
    Sprite davidS;
    Jotaro.fighterText.loadFromFile(JOTARO);
    Jotaro.fighter.setTexture(Jotaro.fighterText);
    Jotaro.fighter.setTextureRect(IntRect(6, 34, 37, 57));
    Jotaro.fighter.setOrigin(37/2, 57/2);
    Jotaro.fighter.setScale(2,2);
    Jotaro.fighter.setPosition(50,0);
    Jotaro.stand.setTexture(Jotaro.fighterText);
    Jotaro.stand.setOrigin(38, 62);
    Jotaro.stand.setScale(2,2);
    davidT.loadFromFile(JOTARO);
    davidS.setTexture(davidT);
    davidS.setTextureRect(IntRect(6, 34, 37, 57));
    davidS.setOrigin(37/2, 57/2);
    davidS.setScale(2,2);
    davidS.setPosition(500,500);

    RectangleShape sol(Vector2f(1300, 15));
    sol.setPosition(0, 600);
    sol.setFillColor(Color::Yellow);

    while (app.isOpen())
    {

        Event event;
        if (Event::Closed)
                app.close();
        if (app.pollEvent(event))
        {

        }
        input(event, &Jotaro, sol, Keyboard::D, Keyboard::Q, Keyboard::Z,  Keyboard::S,  Keyboard::R,  Keyboard::T);
        if (!isOnGround(Jotaro.fighter, sol))
        {
            //printf("%i\n",Jotaro.jump);

            Jotaro.fighter.move(Vector2f(gravity.x, Jotaro.velocity.y + gravity.y*deltaTime.getElapsedTime().asSeconds()));
            Jotaro.fighterAnim = 6;
        }
        else
        {
            deltaTime.restart();
            if (Jotaro.velocity.x == 0)
                Jotaro.fighterAnim = 0;
        }

        animation(&Jotaro.fighter, &Jotaro.animation[Jotaro.fighterAnim], &Jotaro.fighterCl);
        animation(&Jotaro.stand, &Jotaro.animation[Jotaro.standAnim], &Jotaro.standCl);

        Jotaro.fighter.move(Jotaro.velocity);
        Jotaro.stand.setPosition(Jotaro.fighter.getPosition().x+Jotaro.standOffset, Jotaro.fighter.getPosition().y+37); // On ajoute la taille de Jotaro
        davidS.move(velocityDavid);
        hit(&Jotaro, &Jotaro);
        app.clear();
        app.draw(Jotaro.fighter);
        app.draw(davidS);
        if (Jotaro.guardActive == 1 || Jotaro.standActive == 1)
        {
            app.draw(Jotaro.stand);
        }
        app.draw(sol);
        app.display();

        }
    return EXIT_SUCCESS;
    }
