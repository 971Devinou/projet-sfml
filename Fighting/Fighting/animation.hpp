#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <string.h>
#define JOTARO "jotaro_spritesheet_correct.png"

using namespace sf;

typedef struct
{
    int offsetX;
    IntRect animCoord;
    int frameLimit;
    float delayMillis;
    //SoundBuffer animBuffer;
} Anim;

typedef struct
{
    bool attack = false;
    bool attackStand = false;
} AttackType;

typedef struct
{
    int id;
    char nom[20];
    int vie = 500;
    int VIE_MAX = 500;
    int fighterAnim = 0;
    int standAnim = 0;
    Sprite fighter;
    Sprite stand;
    Clock fighterCl;
    Clock standCl;
    bool guardActive = 0;
    bool standActive = 0;
    Vector2f velocity = Vector2f(0.f,0.f);
    int standOffset = 0;
    Texture fighterText;
    Anim animation[100];
    AttackType attackMode;
    //int fighterFrame = 0;
    //Sound fighterSound;
} Fighter;

void animation(Sprite *sprite, Anim *coord, Clock *clock);
void hit(Fighter *fighterOne, Fighter *fighterTwo);
bool isOnGround(Sprite object, RectangleShape ground);
void input(Event evenement, Fighter *player, RectangleShape sol ,Keyboard::Key moveRight, Keyboard::Key moveLeft, Keyboard::Key jump, Keyboard::Key guard, Keyboard::Key attack, Keyboard::Key specialAttack);
Anim setAnimation(IntRect coord, int frameLimit, float delayInterFrames);
