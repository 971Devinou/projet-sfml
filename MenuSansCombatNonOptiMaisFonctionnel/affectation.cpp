#include "gen.h"

using namespace sf;
/*
void animation(Sprite *sprite, Anim *coord, Clock *clock){
     if(clock->getElapsedTime().asMilliseconds()  > coord->delayMillis)
        {
            if(coord->animCoord.left > (coord->frameLimit-2)*(coord->animCoord.width)+coord->offsetX)
            {
                coord->animCoord.left = coord->offsetX;
            }

            else
                coord->animCoord.left += coord->animCoord.width;
            clock->restart();
        }
        sprite->setTextureRect(coord->animCoord);
}

void hit(Fighter *fighterOne, Fighter *fighterTwo)
{
    Fighter fighterOneTemp = *fighterOne;
    Fighter fighterTwoTemp = *fighterTwo;

    if (fighterOneTemp.attackMode.attack == true || fighterOneTemp.attackMode.attackStand == true || fighterTwoTemp.attackMode.attack == true || fighterTwoTemp.attackMode.attackStand == true)
    {
        printf("ORA ");
    }
    *fighterOne = fighterOneTemp;
    *fighterTwo = fighterTwoTemp;
}

bool isOnGround(Sprite object, RectangleShape ground)
{
    bool results = false;
    if (object.getGlobalBounds().intersects(ground.getGlobalBounds()))
    {
        results = true;
    }
    return results;
}

void input(Event evenement, Fighter *player, RectangleShape sol ,Keyboard::Key moveRight, Keyboard::Key moveLeft, Keyboard::Key jump, Keyboard::Key guard, Keyboard::Key attack, Keyboard::Key specialAttack)
{
    Fighter theFighter = *player;
    switch (evenement.type)
            {
            case Event::KeyPressed:
                if (evenement.key.code == moveRight)
                {

                    theFighter.velocity.x = PAS;
                    theFighter.fighterAnim = 1;
                    theFighter.fighter.setScale(2,2);
                    theFighter.stand.setScale(2,2);
                    theFighter.standOffset = STAND_OFFSET;

                    theFighter.guardActive = 0;
                }
                else if (evenement.key.code == moveLeft)
                {
                    theFighter.velocity.x = PAS_MINUS;
                    theFighter.fighter.setScale(-2,2);
                    theFighter.stand.setScale(-2,2);
                    theFighter.standOffset = STAND_OFFSET_MINUS;
                    theFighter.fighterAnim = 1;
                    theFighter.guardActive = 0;

                }
                else if (evenement.key.code == guard && theFighter.velocity.x == 0)
                {
                    theFighter.fighterAnim = 2;
                    theFighter.standAnim = 3;
                    theFighter.guardActive = 1;
                    theFighter.standActive = 0;
                }
                else if (evenement.key.code == specialAttack && theFighter.velocity.x == 0)
                {
                    theFighter.fighterAnim = 0;
                    theFighter.velocity.x = 0;
                    theFighter.standAnim = 4;
                    theFighter.standActive = 1;
                    theFighter.guardActive = 0;
                    theFighter.attackMode.attackStand = true;
                }
                if (evenement.key.code == jump && isOnGround(theFighter.fighter, sol))
                {
                        theFighter.velocity.y = -5.f;
                        theFighter.fighterAnim = 5;

                }
                if (!isOnGround(theFighter.fighter, sol))
                    theFighter.velocity.y = 0;

                break;
            case Event::KeyReleased:
                if (evenement.key.code == moveRight || evenement.key.code == moveLeft)
                {
                    theFighter.velocity.x = 0;
                    theFighter.guardActive = 0;

                }
                if (evenement.key.code == guard)
                {
                    theFighter.guardActive = 0;
                    theFighter.standActive = 0;
                }
                if (evenement.key.code == specialAttack)
                {
                    theFighter.guardActive = 0;
                    theFighter.standActive = 0;
                    theFighter.attackMode.attackStand = false;
                }
                if (evenement.key.code == jump)
                {
                    theFighter.velocity.y = 0;
                }

                if (theFighter.velocity.x == 0)
                    theFighter.fighterAnim = 0;

                break;
            default:
                break;
            }
            *player = theFighter;
}

Anim setAnimation(IntRect coord, int frameLimit, float delayInterFrames)
{
    Anim results;
    results.animCoord = coord;
    results.delayMillis = delayInterFrames;
    results.frameLimit = frameLimit;
    results.offsetX = coord.left;
    return results;
}
*/

