#include "gen.h" // dans gen.h il y a tous les liens vers nos autres fichiers CPP

using namespace sf; // On utilise l'environnement SF
using namespace std;

Sprite iconePersonnage [NOMBRE_DE_PERSO];
Texture textureIconePerso[NOMBRE_DE_PERSO];

RectangleShape barreVie1;
RectangleShape barreVie2;

int boucle = 0;

Fighter joueur1;
Fighter joueur2;

Clock montre;
int start, stop;

Map maps;

int mapId = -1;

RenderWindow fen;
int main()
{



    // la variable selectionMenu permet de surligner les boutons et de selectionner ensuite els menu dans la variable menu
    // La varible selecetionMap permet la selection de la Map surlaquelle on va jouer
    // la variable selectionPerso permet la selection du Personnage

    //int selectionMap = 0, selectionMenu = 0, selectionPerso = 0;

    // mapId permet la d�signation de la map
    // menu permet la designation du menu dans lequel on se trouve
    //int  menu = 0; // La valeur -1 d�signe le fait que la map n'est pas initialis�e.

    /*
        Voici les valeurs des menus (Celle-ci on �t� affect�es de mani�re croissante, d�s que le menu a eu besoin d'�tre cr��):
            -> 0 : Menu Principale
            -> 1 : Menu de selection de Map
            -> 2 : Menu d'Option
            -> 3 : Menu Cr�dits
            -> 4 : Cette valeur correspond au boutton quitter du programme :
                On arrete le programme si le menu = 4
            -> 5 : Menu de selection des personnages
            -> 6 : Interface de jeu
    */

    // Les idPerso permettent de savoir quel perso les joueurs ont choisi.
    //int idPersoJ1 = -1, idPersoJ2 = -1; // La valeur -1 d�signe le fait que les perso ne sont pas initialis�s.
    //int idJoueurActif = 1;


    // Un simple test en combat

    int selectionMap = 1, selectionMenu = 6, selectionPerso = 0;
    int mapId = 2, menu = 6;
    int idPersoJ1 = 3, idPersoJ2 = 0;
    int idJoueurActif = 1;


    /*
           Initialisation des sons

            --------------------------------------------------------------------------------------

             On cr�e un sound effect pour chaque clic
        */
    SoundBuffer bufferSelect;
    if (!bufferSelect.loadFromFile(SON_SELECT))  // Si l'on ne trouve pas le son, on affice une erreur
    {
        printf("Le fichier son : %s est introuvable", SON_SELECT);
        exit(2);
    }

    Sound soundSelect;
    soundSelect.setBuffer(bufferSelect);

    Music musiqueFond; // On met en place la musique de fond
    if (!musiqueFond.openFromFile(MUSIQUE_FOND))
    {
        printf("Impossible de mettre le son : %s\n", MUSIQUE_FOND);
        exit(4);
    }

    musiqueFond.setLoop(true); // On cr�e une loop
    musiqueFond.setVolume(50.0); // On baisse le volume
    musiqueFond.play(); // On commance � joueur la musique

    /*
        ---------------------------------------------------------------------------------------
    */


    Font f; // On met en place la police d'�criture
    if (!f.loadFromFile(FONT)) // On v�rifie qu'elle existe
    {
        printf("Erreur Impossible de charger la police"); // si l    police n'existe pas, on le dit
        exit(0);
    }

    /*
        ---------------------------------------------------------------------------------------
    */



    //RenderWindow fen(VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Jojo Star Fighter"); // C'est la fen�tre principale

    /*

        fen.setFramerateLimit(FPS); // on fixe la limite des FPS � 120

        while (fen.isOpen()) // Tant que la fenetre est ouverte
        {
            Event event;
            while (fen.pollEvent(event)) // On prend tout les evenements
            {
                switch(event.type)
                {
                case Event::Closed : // si on veut fermer
                    musiqueFond.stop(); // alors on arrete la musique
                    fen.close(); // on ferme
                    break; // et on quitte la boucle

                case Event::MouseMoved: // On met en place les diff�rentes selections dans les diff�rentes menus
                    if (menu == 0) // Si on est dans le menu principale
                        selectionMenu = choixMenuP(event.mouseMove.x,event.mouseMove.y); // on utilise la selection sp�cifique � ce menu
                    else if (menu == 1)  // Menu selection Map
                    {
                        selectionMap = choixMap(event.mouseMove.x, event.mouseMove.y);
                        selectionMenu = detectionChangementMenu(event.mouseMove.x, event.mouseMove.y, 0, 1, 5, selectionMenu);
                    }
                    else if (menu == 2) // Menu Options
                        selectionMenu = detectionRetourMenuPrecedent(event.mouseMove.x,event.mouseMove.y, 2, 0, selectionMenu);
                    else if(menu == 3) // Menu Cr�dits
                        selectionMenu = detectionRetourMenuPrecedent(event.mouseMove.x,event.mouseMove.y, 3, 0, selectionMenu);
                    else if(menu == 5) // Menu selection Perso
                    {
                        selectionPerso = choixPerso(event.mouseMove.x, event.mouseMove.y);
                        selectionMenu = detectionChangementMenu(event.mouseMove.x,event.mouseMove.y, 1, 5, 6, selectionMenu);
                    }
                    break; // on quitte le switch

                case Event::MouseButtonPressed: // Si on clique sur un menu, Alors on r�agit en cons�quence
                    if (selectionMenu == 4)   // Si on clique sur le bouton Quitter
                    {
                        fen.close(); // La fenetre s'arrete
                        musiqueFond.stop(); // La musique s'arrete
                    }

                    if (menu == 1 && selectionMap != -1) // Si on clique sur une map et que cette selection n'est pas invalide ...
                        mapId = selectionMap; // Alors on confirme la map de eju

                    if (menu == 5 )  // Si l'on est le menu de selection des personnages
                    {
                        if( selectionPerso != -1)  // si la selection perso n'est pas invalide
                        {
                            if (idJoueurActif == 1) // Si le joueur qui est en cour de selection est le permier
                                idPersoJ1 = selectionPerso; // Alors on confirme sa selection du joueur 1
                            else // Si le joueur qui est en cour de selection est le deuxi�me
                                idPersoJ2 = selectionPerso; // Alors on confirme sa selection de joueur 2
                        }
                    }

                    menu = selectionMenu; // de mani�re g�n�rale, la selectionMenu correpond � l'id du menu que l'on veut visiter

                    /*
                        Si le joueur veut aller dans la selection de personnage ( en fait selectionMenu = 5, cela veut dire,
                        qu'avant la ligne 139, menu = 1)
                        et que la mapID n'est pas valide (Pas selectionn� par le joueur)...
                    */
    /*
    if ((menu == 5 && mapId < 0))
    {
        menu = 1; // Alors on interdit le fait de pouvoir changer de menu
    } // Cela permet de ne pas aller en combat sans avoir selection� de map

    /*
        Si les joueurs veulent aller en combat  (menu = 6) mais les ids des persos ne sont pas valides
        (Les joueurs n'ont pas selection� de persos), alors on retourne au menu 5;
    */
    /*
    else if (menu == 6 &&((idPersoJ1 < 0) || (idPersoJ2 < 0)))
    {
        menu = 5;
    } // Cela permet de ne pas aller en combat sans personnages

    soundSelect.play(); // On joue le son de selection � chaque fois que l'on clique quelque part
    break; // On quitte le switch

    case Event::MouseWheelMoved: // Cet input permet de changer de "joueur actif", c'est � dire que
    // Si On enclenche cet evenement, on pourra passer de J1 � J2
    if (idJoueurActif ==1) // Si J1 �tait la avant que l'on tourne la roue de la souris,
        idJoueurActif = 2; // Alors C'est d�sormais le J2 qui a la main (pour la selection des persos)
    else
        idJoueurActif = 1;
    break;

    default : // On ne prend en compte aucun autre ev�nement
    break;
    }

    }
    printf("menu : %i\n", menu);

    if (menu == 0) // Si on est dans le menu d'accueil, on l'affiche
    {
    //RectangleShape jouer = creationBouton()
    printf("in\n");
    afficherMenuP(fen, selectionMenu, f);
    }
    else if (menu == 1) // Menu Selection de Map
    {
    afficherSelectionMap(fen, selectionMap, mapId, selectionMenu, f);
    }
    else if (menu == 2) // Menu Options
    afficherMenuO(fen, selectionMenu, f);

    else if (menu == 3) // Menu Credits
    afficherMenuC(fen, selectionMenu, f);

    else if (menu == 5)  // Menu Selection de Personnages
    {
    afficherSelectionPerso(fen, selectionPerso, idPersoJ1, idPersoJ2, selectionMenu, f);
    afficherCurseurJoueur(idJoueurActif, (float)SCREEN_WIDTH / 5, (float)SCREEN_HEIGHT / 2 - 50, f, fen);
    }

    else if (menu == 6) // Interface de jeu
    {
    fen.close();


    }

    fen.display();

    }
    */
    RenderWindow jeu(VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Jojo Star Fighter"); // C'est la fen�tre principale
    initFighters(1, &joueur1);
    initFighters(2, &joueur2);

    initMaps(0, &maps);

    Sprite iconeJoueur1 = iconePersonnage[idPersoJ1];
    iconeJoueur1.setPosition(0,0);
    iconeJoueur1.scale( 100.0 / textureIconePerso[idPersoJ1].getSize().x, 100.0 / textureIconePerso[idPersoJ1].getSize().y);

    Sprite iconeJoueur2 = iconePersonnage[idPersoJ2];
    iconeJoueur2.setPosition(SCREEN_WIDTH - 100, 0);
    iconeJoueur2.scale( 100.0 / textureIconePerso[idPersoJ2].getSize().x, 100.0 / textureIconePerso[idPersoJ2].getSize().y);

    RectangleShape sol(Vector2f(SCREEN_WIDTH, 50));
    sol.setPosition(0,SCREEN_HEIGHT);
    sol.setFillColor(Color(0,0,0,0));

    const float maxY = 50.f;
    const Vector2f gravity(0.f, 8.f);
    Vector2f velocity(2.f, 5.f);
    int inGame = 1;

    Event event;

    while(jeu.isOpen())
    {



        while(fen.pollEvent(event))
        {
            switch(event.type)
            {
            case Event::Closed:
                inGame = 0;
                fen.close();
                break;
            case Event::MouseButtonPressed:
                if (joueur1.vie <= 0 || joueur2.vie <= 0)
                {
                    inGame = detectionRetourMenuPrecedent(event.mouseButton.x, event.mouseButton.y, 1, 0, 1);
                }
            case Event::KeyPressed:

                if (event.key.code == Keyboard::Escape)
                    inGame = pause(f, jeu);
            default :
                break;
            }

        }


        input(event, &joueur1, sol, Keyboard::D, Keyboard::Q, Keyboard::Z, Keyboard::S, Keyboard::R, Keyboard::T);
        input(event, &joueur2, sol, Keyboard::Right, Keyboard::Left, Keyboard::Up, Keyboard::Down, Keyboard::Numpad0, Keyboard::Numpad2);

        stop = montre.getElapsedTime().asMilliseconds();

        printf("boucle\n");

        jeu.clear();
        //affichageMap(mapId,jeu);
        jeu.draw(iconeJoueur1);
        jeu.draw(iconeJoueur2);

        //affichageVie(joueur1.vie,joueur2.vie,jeu);

        if(joueur1.vie <= 0 || joueur2.vie<= 0)
        {
            afficheGagnant(joueur1.vie, joueur2.vie, f, jeu);
            afficherBoutonQuitter(jeu, 0, f, 0, "Retour");
        }


        if (!isOnGround(joueur1.fighter, sol))
        {
            joueur1.fighter.move(gravity);
            joueur1.fighterAnim = IN_AIR;
        }
        else
        {
            if (joueur1.velocity.x == 0 && (joueur1.attackMode.attack == false && joueur1.attackMode.attackStand == false))
            {
                joueur1.fighterAnim = IDLE;
            }
        }

        if (!isOnGround(joueur2.fighter, sol))
        {
            joueur2.fighter.move(gravity);
            joueur2.fighterAnim = IN_AIR;
        }
        else
        {
            if (joueur2.velocity.x == 0 && (joueur2.attackMode.attack == false && joueur2.attackMode.attackStand == false))
            {
                joueur2.fighterAnim = IDLE;
            }

        }

        animation(&joueur1.fighter, &joueur1.animation[joueur1.fighterAnim], &joueur1.fighterCl);
        animation(&joueur1.stand, &joueur1.animation[joueur1.standAnim], &joueur1.standCl);


        hit(&joueur1, &joueur2);
        if (joueur2.vie > 0 )
        {
            animation(&joueur2.fighter, &joueur2.animation[joueur2.fighterAnim], &joueur2.fighterCl);
        }
        else
        {

            //animation(&joueur2.fighter, &joueur2.animation[BATTU], &joueur2.fighterCl);
            //joueur2.controlAcces = false;
            joueur2.isAlive = false;
            trueAnimation(&joueur2, FALL, 1);
            joueur2.isAlive = false;
            //printf("/////%i %i", joueur2.canAnim, joueur2.animBegin);
            //system("PAUSE");
        }

        //printf("|||||||||||%i %i\n", joueur2.canAnim, joueur2.animBegin);

        animation(&maps.mapSprite, &maps.animation, &maps.montreSprite);
        jeu.draw(maps.mapSprite);

        jeu.draw(joueur1.fighter);
        jeu.draw(joueur2.fighter);
        if ((joueur1.guardActive == 1 || joueur1.standActive == 1) && joueur1.externStand == true)
        {
            jeu.draw(joueur1.stand);
        }
        jeu.display();
    }

    return EXIT_SUCCESS;
}



//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////


void initImgs()
{
    barreVie2.setSize(Vector2f(250,10));
    barreVie2.setPosition(1053,100);
    barreVie2.setFillColor(Color :: Green);

    barreVie1.setSize(Vector2f(250,10));
    barreVie1.setPosition(0,100);
    barreVie1.setFillColor(Color :: Green);

    textureIconePerso[0].loadFromFile(PERSO_AVDOL);
    textureIconePerso[1].loadFromFile(PERSO_DIO);
    textureIconePerso[2].loadFromFile(PERSO_JOSEPH);
    textureIconePerso[3].loadFromFile(PERSO_JOSUKE);
    textureIconePerso[4].loadFromFile(PERSO_JOTARO);
    textureIconePerso[5].loadFromFile(PERSO_KAKYOIN);
    textureIconePerso[6].loadFromFile(PERSO_POLNAREFF);
    textureIconePerso[7].loadFromFile(PERSO_HOLHORSE);

    int i;
    for (i = 0; i < NOMBRE_DE_PERSO; i++)
        iconePersonnage[i].setTexture(textureIconePerso[i]);

}

void affichageMap(Map maps, RenderWindow &app)
{
    animation(&maps.mapSprite, &maps.animation, &maps.montreSprite);
    app.draw(maps.mapSprite);

}
void affichageVie(int vieJ1, int vieJ2, RenderWindow &app)
{
    float longeurBarreJ1 = vieJ1 * 250.0 / MAX_HP;
    float longeurBarreJ2 = vieJ2 * 250.0 / MAX_HP;

    float pourcentageRougeJ1, pourcentageVertJ1;
    pourcentageVertJ1 = vieJ1 * 255 / MAX_HP;
    pourcentageRougeJ1 = 255 - pourcentageVertJ1;

    float pourcentageRougeJ2, pourcentageVertJ2;
    pourcentageVertJ2 = vieJ2 * 255 / MAX_HP;
    pourcentageRougeJ2 = 255 - pourcentageVertJ2;

    barreVie1.setSize(Vector2f(longeurBarreJ1, 10));
    barreVie2.setSize(Vector2f(longeurBarreJ2, 10));

    barreVie1.setFillColor(Color(pourcentageRougeJ1, pourcentageVertJ1, 0));
    barreVie2.setFillColor(Color(pourcentageRougeJ2, pourcentageVertJ2, 0));

    barreVie2.setPosition(SCREEN_WIDTH - longeurBarreJ2, 100);

    app.draw(barreVie1);
    app.draw(barreVie2);
}
void afficheGagnant(int vieJ1, int vieJ2, Font f, RenderWindow &app)
{

    Text KO;
    KO.setCharacterSize(50);
    KO.setColor(Color::Red);
    KO.setFont(f);
    KO.setString("KO");
    KO.setPosition(SCREEN_WIDTH / 2 - 50, SCREEN_HEIGHT / 4);

    Text vainqueur;
    vainqueur.setCharacterSize(50);
    vainqueur.setColor(Color::Red);
    vainqueur.setFont(f);
    if (vieJ1 <= 0 && vieJ2 <= 0)
    {
        vainqueur.setString("Egalite");
        vainqueur.setPosition(SCREEN_WIDTH / 2 - 50 * 3.5, SCREEN_HEIGHT / 2);
    }
    else
    {
        vainqueur.setPosition(SCREEN_WIDTH / 2 - 100, SCREEN_HEIGHT / 2);
        if(vieJ1 <= 0)
            vainqueur.setString("J2 Gagne");
        else
            vainqueur.setString("J1 Gagne");
    }
    app.draw(KO);
    app.draw(vainqueur);
}

void animation(Sprite *sprite, Anim *coord, Clock *clock)
{
    if(clock->getElapsedTime().asMilliseconds()  > coord->delayMillis)
    {
        if(coord->animCoord.left > (coord->frameLimit-2)*(coord->animCoord.width)+coord->offsetX)
        {
            coord->animCoord.left = coord->offsetX;
        }

        else
            coord->animCoord.left += coord->animCoord.width;
        clock->restart();
    }
    sprite->setTextureRect(coord->animCoord);
    //sprite->setOrigin(coord->animCoord.width/2, coord->animCoord.height);
}
/*
void animation(Sprite *sprite, Anim *coord, Clock *clock)
{
    if(clock->getElapsedTime().asMilliseconds()  > coord->delayMillis)
    {
        if(coord->animCoord.left > (coord->frameLimit-2)*(coord->animCoord.width)+coord->offsetX)
        {
            coord->animCoord.left = coord->offsetX;
        }

        else
            coord->animCoord.left += coord->animCoord.width;
        clock->restart();
    }
    sprite->setTextureRect(coord->animCoord);
}
*/



void hit(Fighter *fighterOne, Fighter *fighterTwo)
{
    Fighter fighterOneTemp = *fighterOne;
    Fighter fighterTwoTemp = *fighterTwo;
    int degatsOne = 0, degatsTwo = 0;

    if (fighterOneTemp.attackMode.attack == true || fighterOneTemp.attackMode.attackStand == true || fighterTwoTemp.attackMode.attack == true || fighterTwoTemp.attackMode.attackStand == true)
    {

        if (fighterOneTemp.externStand == true) //Si le joueur 1 (stand d�tach�) frappe le joueur 2 avec une attaque sp�ciale
        {
            if (fighterOneTemp.standActive == true && fighterOneTemp.attackQualification.special == true && fighterOneTemp.attackMode.attackStand == true)
            {
                if(fighterOneTemp.stand.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                {
                    degatsTwo = DEGATS_SUPER;
                    fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                }
            }
        }

        if (fighterTwoTemp.externStand == true) //Si le joueur 2 (stand d�tach�) frappe le joueur 1 avec une attaque sp�ciale
        {
            if (fighterTwoTemp.standActive == true && fighterTwoTemp.attackQualification.special == true && fighterTwoTemp.attackMode.attackStand == true)
            {
                if(fighterTwoTemp.stand.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                {
                    degatsOne = DEGATS_SUPER;
                    fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                }
            }
        }

        if (fighterOneTemp.externStand == false) // Attaques sp�ciales stand attach�
        {
            if (fighterOneTemp.attackQualification.special == true && fighterOneTemp.attackMode.attack == true)
            {
                if(fighterOneTemp.fighter.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
                {
                    degatsTwo = DEGATS_SUPER;
                    fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
                }
            }
        }

        if (fighterTwoTemp.externStand == false) // Attaques sp�ciales stand attach�
        {
            if (fighterTwoTemp.attackQualification.special == true && fighterTwoTemp.attackMode.attack == true)
            {
                if(fighterTwoTemp.fighter.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
                {
                    degatsOne = DEGATS_SUPER;
                    fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
                }
            }
        }
        if (fighterOneTemp.standActive == false && fighterOneTemp.attackQualification.normal == true && fighterOneTemp.attackMode.attack == true)
        {
            if(fighterOneTemp.fighter.getGlobalBounds().intersects(fighterTwoTemp.fighter.getGlobalBounds()))
            {
                degatsTwo = DEGATS_NORMAL;
                fighterTwoTemp.fighter.move(fighterOneTemp.fighter.getScale().x/2*1, 0);
            }
        }
        if (fighterTwoTemp.standActive == false && fighterTwoTemp.attackQualification.normal == true && fighterTwoTemp.attackMode.attack == true)
        {
            if(fighterTwoTemp.fighter.getGlobalBounds().intersects(fighterOneTemp.fighter.getGlobalBounds()))
            {
                degatsOne = DEGATS_NORMAL;
                fighterOneTemp.fighter.move(fighterTwoTemp.fighter.getScale().x/2*1, 0);
            }
        }

    }
    if (fighterOneTemp.guardActive == true)
        degatsOne = degatsOne/2;
    if (fighterTwoTemp.guardActive == true)
        degatsTwo = degatsTwo/2;

    fighterOneTemp.vie -= degatsOne;
    fighterTwoTemp.vie -= degatsTwo;
    *fighterOne = fighterOneTemp;
    *fighterTwo = fighterTwoTemp;
}

bool isOnGround(Sprite object, RectangleShape ground) // A mettre un ground transparent
{
    bool results = false;
    if (object.getGlobalBounds().intersects(ground.getGlobalBounds()))
    {
        results = true;
    }
    return results;
}

void input(Event evenement, Fighter *player, RectangleShape sol,Keyboard::Key moveRight, Keyboard::Key moveLeft, Keyboard::Key jump, Keyboard::Key guard, Keyboard::Key attack, Keyboard::Key specialAttack)
{
    Fighter theFighter = *player;
    const float maxY = 50.f;
    const Vector2f gravity(0.f, 8.f);
    Vector2f velocity(2.f, 5.f);

    if (theFighter.controlAcces == true)
    {
        switch (evenement.type)
        {
        case Event::KeyPressed:
            if (evenement.key.code == moveRight)
            {

                theFighter.velocity.x = PAS;
                theFighter.fighterAnim = MOVE;
                theFighter.fighter.setScale(2,2);

                if (theFighter.externStand == true)
                {
                    //theFighter.standOffset = STAND_OFFSET;
                    theFighter.stand.setScale(2,2);
                }
                theFighter.guardActive = false;
                theFighter.attackQualification.special = false;
                theFighter.attackMode.attack = false;
                if(theFighter.externStand == true)
                {
                    theFighter.standActive = false;
                    theFighter.attackMode.attackStand = false;
                }

            }
            else if (evenement.key.code == moveLeft)
            {
                theFighter.velocity.x = PAS_MINUS;
                theFighter.fighter.setScale(-2,2);
                theFighter.fighterAnim = MOVE;

                if (theFighter.externStand == true)
                {
                    theFighter.stand.setScale(-2,2);
                    //theFighter.standOffset = STAND_OFFSET_MINUS;
                }
                theFighter.guardActive = false;
                theFighter.attackQualification.special = false;
                theFighter.attackMode.attack = false;
                if(theFighter.externStand == true)
                {
                    theFighter.standActive = false;
                    theFighter.attackMode.attackStand = false;
                }
            }
            else if (evenement.key.code == guard && theFighter.velocity.x == 0)
            {
                theFighter.fighterAnim = GUARD;
                theFighter.guardActive = true;
                if (theFighter.externStand == true)
                {
                    theFighter.standAnim = STAND_GUARD;
                    theFighter.standActive = false;
                }

            }
            else if (evenement.key.code == attack && theFighter.velocity.x == 0)
            {
                theFighter.guardActive = false;
                theFighter.attackQualification.normal = true;
                theFighter.attackMode.attack = true;
                if (theFighter.attackInst == false)
                {
                    theFighter.fighterAnim = NORMAL_ATTACK_ONE;
                    //theFighter.attackInst = !theFighter.attackInst;
                }
                else if (theFighter.attackInst == true)
                {
                    theFighter.fighterAnim = NORMAL_ATTACK_TWO;
                    //theFighter.attackInst = !theFighter.attackInst;
                }

            }
            else if (evenement.key.code == specialAttack && theFighter.velocity.x == 0)
            {
                //theFighter.fighterAnim = 0;
                theFighter.velocity.x = 0;
                theFighter.guardActive = false;
                theFighter.attackMode.attack = true;
                theFighter.attackQualification.special = true;

                if (theFighter.externStand == true)
                {
                    theFighter.fighterAnim = IDLE;
                    theFighter.standAnim = SPECIAL;
                    theFighter.standActive = true;
                    theFighter.attackMode.attackStand = true;
                }
                else if (theFighter.externStand == false)
                {
                    theFighter.fighterAnim = SPECIAL;
                    theFighter.attackMode.attack = true;
                }

            }
            if (evenement.key.code == jump && isOnGround(theFighter.fighter, sol))
            {
                theFighter.fighterAnim = JUMP_START;
                theFighter.jump = true;
                theFighter.velocity.y = -10.0f;
            }


            break;
        case Event::KeyReleased:
            if (evenement.key.code == moveRight || evenement.key.code == moveLeft)
            {
                theFighter.velocity.x = 0;
                theFighter.guardActive = false;

            }
            if (evenement.key.code == guard)
            {
                theFighter.guardActive = false;

                if (theFighter.externStand == true)
                    theFighter.standActive = false;
            }

            if (evenement.key.code == attack)
            {
                theFighter.guardActive = false;
                theFighter.attackMode.attack = false;
                theFighter.attackQualification.normal = false;
            }

            if (evenement.key.code == specialAttack)
            {
                theFighter.guardActive = false;
                theFighter.attackQualification.special = false;
                theFighter.attackMode.attack = false;
                if(theFighter.externStand == true)
                {
                    theFighter.standActive = false;
                    theFighter.attackMode.attackStand = false;
                }
            }
            if (evenement.key.code == jump)
            {
                theFighter.velocity.y = IDLE;
            }

            if (theFighter.velocity.x == 0)
                theFighter.fighterAnim = IDLE;

            break;
        default:
            break;
        }
    }
    else
    {
        printf("Acces a moi desactive\n");
        theFighter.velocity.x = 0;
    }
    theFighter.fighter.move(theFighter.velocity);
    if (theFighter.externStand == true)
    {
        theFighter.standOffset = Vector2f(theFighter.animation[theFighter.fighterAnim].animCoord.width/2, theFighter.animation[theFighter.fighterAnim].animCoord.height/2);
        theFighter.stand.setPosition(theFighter.fighter.getPosition().x+theFighter.standOffset.x*theFighter.fighter.getScale().x*2, theFighter.fighter.getPosition().y);
    }

    *player = theFighter;
}


void initFighters(int i, Fighter *persoChoisi)
{
    switch(i)
    {
    case 0 :
        persoChoisi->externStand = true;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_AVDOL)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_AVDOL);
            exit(IMG_NON_EXISTANTE);
        }

        // Avdol
        persoChoisi->animation[IDLE] = setAnimation(IntRect(26,31,44,64), 3, 250.0f);
        persoChoisi->animation[MOVE] = setAnimation(IntRect(14,192,54,63), 8, 250.0f);
        persoChoisi->animation[GUARD] = setAnimation(IntRect(25,110,63,59), 2, 250.0f);
        //persoChoisi->animation[3] = setAnimation(IntRect(0,0,33,59), 4, 250.0f);
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(32,299,45,69), 2, 250.0f);
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(126,297,45,71), 2, 250.0f);
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(216,299,45,69), 2, 250.0f);
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(2,667,68,65), 4, 250.0f);
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(7,578,57,75), 4, 250.0f);
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(12,1013,67,121), 7, 250.0f);
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(25,916,65,61), 4, 250.0f);
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(28,1197,107,92), 7, 250.0f);
        persoChoisi->animation[HIT] = setAnimation(IntRect(162,402,68,59), 7, 250.0f);
        persoChoisi->animation[FALL] = setAnimation(IntRect(10,400,68,61), 9, 250.0f);
        persoChoisi->animation[BATTU] = setAnimation(IntRect(10,400,68,61), 1, 250.0f);
        break;


    case 1 :
        persoChoisi->externStand = false;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_DIO)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_DIO);
            exit(IMG_NON_EXISTANTE);
        }

        //Dio
        persoChoisi->animation[IDLE] = setAnimation(IntRect(1, 7, 25, 64), 10, 100.0); //idle
        persoChoisi->animation[MOVE] = setAnimation(IntRect(2, 102, 53, 47), 4, 100.0); //walk
        persoChoisi->animation[GUARD] = setAnimation(IntRect(23, 1635, 28, 65), 3, 100.0); //Guard
        // persoChoisi->animation[3] = setAnimation(IntRect(0, 0, 0, 0), 0, 100.0) //StandGuard
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(4, 221, 48, 71), 3, 100.0); //start jump
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(100, 221, 48, 71), 1, 100.0); //jump in air
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(196, 221, 48, 71), 4, 100.0); //end jump
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(1, 439, 60, 63), 9, 100.0); //attaque1
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(1, 1316, 60, 65), 9, 100.0); //attaque2
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(1, 840, 68, 62), 6, 100.0); //specialHaut
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(1, 1494, 101, 86), 7, 100.0); //Special
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(1, 1129, 69, 58), 5, 100.0); //SpecialBas
        persoChoisi->animation[HIT] = setAnimation(IntRect(123, 3328, 61, 66), 2, 100.0); //EstTap�
        persoChoisi->animation[FALL] = setAnimation(IntRect(245, 3328, 61, 66), 7, 100.0); //Tombe
        persoChoisi->animation[BATTU] = setAnimation(IntRect(306, 3328, 61, 66), 1, 100.0); //est a terre
        break;




    case 2 :
        persoChoisi->externStand = true;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_JOSEPH)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_JOSEPH);
            exit(IMG_NON_EXISTANTE);
        }

        //Joseph
        persoChoisi->animation[0] = setAnimation(IntRect(0, 0, 27, 63), 5, 100.0f);
        persoChoisi->animation[1] = setAnimation(IntRect(0, 125, 52, 60), 8, 100.0f);
        persoChoisi->animation[2] = setAnimation(IntRect(0, 63, 39, 62), 4, 100.0f);
// persoChoisi->animation[3] = setAnimation(IntRect(0, 0, 33, 39), 4, 100.0f);
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(0, 185, 41, 65), 2, 200.0f);
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(41, 185, 41, 65), 1, 100.0f);
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(82, 185, 41, 65), 3, 200.0f);
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(0, 550, 58, 64), 5, 100.0f);
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(0, 781, 62, 60), 5, 100.0f);
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(0, 620, 51, 67), 8, 100.0f);
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(0, 1325, 97, 60), 7, 100.0f);
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(0, 1221, 55, 75), 8, 100.0f);
        persoChoisi->animation[HIT] = setAnimation(IntRect(0, 308, 49, 64), 4, 100.0f);
        persoChoisi->animation[FALL] = setAnimation(IntRect(0, 418, 68, 57), 7, 100.0f);
        persoChoisi->animation[BATTU] = setAnimation(IntRect(136, 453, 68, 57), 1, 100.0f);
        break;


    case 3 :
        persoChoisi->externStand = false;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_JOSUKE)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_JOSUKE);
            exit(IMG_NON_EXISTANTE);
        }

        //Josuke
        persoChoisi->animation[IDLE] = setAnimation(IntRect(7, 11, 31, 58), 4, 100.0f);
        persoChoisi->animation[MOVE] = setAnimation(IntRect(31, 156, 47, 44), 8, 100.0f);
        persoChoisi->animation[GUARD] = setAnimation(IntRect(15, 88, 57, 56), 4, 100.0f);
// persoChoisi->animation[3] = setAnimation(IntRect(0, 0, 33, 39), 4, 100.0f);
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(32, 233, 39, 55), 2, 100.0f);
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(110, 233, 39, 55), 1, 100.0f);
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(110, 233, 39, 55), 2, 100.0f);
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(43, 441, 46, 48), 4, 100.0f);
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(28, 567, 42, 57), 6, 100.0f);
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(76, 733, 68, 62), 5, 100.0f);
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(0, 1165, 89, 61), 5, 100.0f);
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(80, 902, 74, 62), 4, 100.0f);
        persoChoisi->animation[HIT] = setAnimation(IntRect(30, 307, 44, 52), 2, 100.0f);
        persoChoisi->animation[FALL] = setAnimation(IntRect(121, 315, 59, 47), 5, 100.0f);
        persoChoisi->animation[BATTU] = setAnimation(IntRect(239, 315, 59, 47), 1, 100.0f);
        break;


    case 4 :
        persoChoisi->externStand = false;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_JOTARO)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_JOTARO);
            exit(IMG_NON_EXISTANTE);
        }

        // Jotaro
        persoChoisi->animation[IDLE] = setAnimation(IntRect(6,34,37,57), 6, 180.0f); //Idle
        persoChoisi->animation[MOVE] = setAnimation(IntRect(647,34,46,57), 8, 70.0f); //Run
        persoChoisi->animation[GUARD] = setAnimation(IntRect(201,141,35,56), 1, 600.0f); //Guard
        persoChoisi->animation[STAND_GUARD] = setAnimation(IntRect(284,135,38,62), 2, 600.0f); //Stand Guard
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(422,140,44,57), 2, 50.0f);// Jump start
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(466, 140, 44, 57), 2, 100.f); //In air
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(598,140,44,57), 4, 50.0f); //Jump end
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(208,936,101,66), 6, 80.0f); //Special
        persoChoisi->animation[HIT] = setAnimation(IntRect(8,234, 40,56), 4, 80.0f); // Hit
        persoChoisi->animation[FALL] = setAnimation(IntRect(228,245, 64,45), 7, 80.0f); // Fall
        persoChoisi->animation[BATTU] = setAnimation(IntRect(356,245, 64,45), 1, 80.0f); // u are ded not a big suprise
        break;


    case 5 :
        persoChoisi->externStand = true;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_KAKYOIN)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_KAKYOIN);
            exit(IMG_NON_EXISTANTE);
        }

        // Kakyoin
        persoChoisi->animation[IDLE] = setAnimation(IntRect(0, 0, 45, 57), 4, 100.0f);
        persoChoisi->animation[MOVE] = setAnimation(IntRect(0, 166, 46, 56), 8, 100.0f);
        persoChoisi->animation[GUARD] = setAnimation(IntRect(0, 92, 46, 58), 4, 100.0f);
// listFighter[5].animation[3] = setAnimation(IntRect(0, 0, 33, 39), 4, 100.0f);
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(0, 270, 44, 59), 3, 100.0f);
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(132, 270, 44, 59), 2, 100.0f);
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(220, 270, 44, 59), 3, 100.0f);
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(3, 457, 53, 58), 4, 100.0f);
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(1, 526, 53, 58), 5, 100.0f);
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(4, 664, 74, 75), 4, 100.0f);
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(4, 774, 162, 62), 4, 100.0f);
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(10, 857, 118, 83), 2, 100.0f);
        persoChoisi->animation[HIT] = setAnimation(IntRect(1, 369, 64, 54), 3, 100.0f);
        persoChoisi->animation[FALL] = setAnimation(IntRect(263, 369, 65, 54), 7, 100.0f);
        persoChoisi->animation[BATTU] = setAnimation(IntRect(393, 369, 65, 54), 1, 100.0f);
        break;


    case 6 :
        persoChoisi->externStand = false;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_POLNAREFF)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_POLNAREFF);
            exit(IMG_NON_EXISTANTE);
        }
        // Polnareff
        persoChoisi->animation[IDLE] = setAnimation(IntRect(0,0,33,59), 4, 250.0f);
        persoChoisi->animation[MOVE] = setAnimation(IntRect(0,181,52,55), 8, 250.0f);
        persoChoisi->animation[GUARD] = setAnimation(IntRect(0,120,59,61), 4, 250.0f);
        //persoChoisi->animation[3] = setAnimation(IntRect(0,0,33,59), 4, 250.0f);
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(0,59,48,61), 7, 250.0f);
        //persoChoisi->animation[5] = setAnimation(IntRect(0,0,33,59), 4, 250.0f);
        //persoChoisi->animation[6] = setAnimation(IntRect(0,0,33,59), 4, 250.0f);
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(0,591,67,57), 5, 250.0f);
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(0,530,61,61), 4, 250.0f);
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(0,428,105,102), 3, 250.0f);
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(0,356,106,72), 3, 250.0f);
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(0,648,87,72), 4, 250.0f);
        persoChoisi->animation[HIT] = setAnimation(IntRect(0,298,46,58), 2, 250.0f);
        persoChoisi->animation[FALL] = setAnimation(IntRect(0,242,71,55), 7, 250.0f);
        persoChoisi->animation[BATTU] = setAnimation(IntRect(142,242,71,55), 1, 250.0f);

        break;


    case 7 :
        persoChoisi->externStand = true;
        if(!(persoChoisi->fighterText.loadFromFile(TEXTURE_HOLHORSE)))
        {
            printf("l'image %s n est pas disponible", TEXTURE_HOLHORSE);
            exit(IMG_NON_EXISTANTE);
        }

        //Hol Horse
        persoChoisi->animation[IDLE] = setAnimation(IntRect(15, 27, 41, 62), 3, 100.0); //idle
        persoChoisi->animation[MOVE] = setAnimation(IntRect(3, 105, 53, 59), 8, 100.0); //walk
        persoChoisi->animation[GUARD] = setAnimation(IntRect(64, 272, 37, 60), 1, 100.0); //Guard
// persoChoisi->animation[3] = setAnimation(IntRect(0, 0, 0, 0), 0, 100.0); //StandGuard
        persoChoisi->animation[JUMP_START] = setAnimation(IntRect(12, 181, 41, 64), 3, 100.0); //start jump
        persoChoisi->animation[IN_AIR] = setAnimation(IntRect(135, 181, 41, 64), 2, 100.0); //jump in air
        persoChoisi->animation[JUMP_END] = setAnimation(IntRect(176, 181, 41, 64), 4, 100.0); //end jump
        persoChoisi->animation[NORMAL_ATTACK_ONE] = setAnimation(IntRect(12, 476, 46, 63), 5, 100.0); //attaque1
        persoChoisi->animation[NORMAL_ATTACK_TWO] = setAnimation(IntRect(2, 940, 56, 68), 6, 100.0); //attaque2
        persoChoisi->animation[UP_SPECIAL] = setAnimation(IntRect(36, 1046, 60, 82), 11, 100.0); //specialHaut
        persoChoisi->animation[SPECIAL] = setAnimation(IntRect(6, 1156, 191, 66), 8, 100.0); //Special
        persoChoisi->animation[DOWN_SPECIAL] = setAnimation(IntRect(0, 1408, 88, 71), 5, 100.0); //SpecialBas
        persoChoisi->animation[HIT] = setAnimation(IntRect(1, 2171, 68, 63), 3, 100.0); //EstTap�
        persoChoisi->animation[FALL] = setAnimation(IntRect(205, 2171, 68, 63), 8, 100.0); //Tombe
        persoChoisi->animation[BATTU] = setAnimation(IntRect(409, 2171, 68, 63), 1, 100.0); //est a terre
        break;


    }

    persoChoisi->fighter.setTexture(persoChoisi->fighterText);
    //listeFighter[i].spriteSheet.setTexture(listeFighter[i].fighterText);
    persoChoisi->stand.setTexture(persoChoisi->fighterText);
    persoChoisi->fighter.scale(3,3);
    persoChoisi->stand.scale(3,3);
    /*
    [0] Idle
    [1] Move
    [2] Guard
    [3] StandGuard
    [4] Jump start
    [5] Jump in ai
    [6] Jump end
    [7] NorlmalAttack1
    [8] NorlmalAttack2
    [9] UpSpecial
    [10] Special
    [11 DownSpecial
    [12] Get Hit
    [13] Fall by hit
    */

}

void initMaps(int idMap, Map *mapChoisie)
{
    switch(idMap)
    {
        case 0:
            mapChoisie->mapTexture.loadFromFile(NOM_ARENE1);
            mapChoisie->animation = setAnimation(IntRect(0,0,800,336), 8, 85.0f);
        case 1:
            mapChoisie->mapTexture.loadFromFile(NOM_ARENE2);
            mapChoisie->animation = setAnimation(IntRect(0,0,768,384), 8, 85.0f);
        case 2:
            mapChoisie->mapTexture.loadFromFile(NOM_ARENE3);
            mapChoisie->animation = setAnimation(IntRect(0,0,752,224), 8, 85.0f);
        case 3 :
            mapChoisie->mapTexture.loadFromFile(NOM_ARENE4);
            mapChoisie->animation = setAnimation(IntRect(0,0,768,256), 8, 85.0f);
        case 4 :
            mapChoisie->mapTexture.loadFromFile(NOM_ARENE5);
            mapChoisie->animation = setAnimation(IntRect(0,0,752,224), 8, 85.0f);

    }



        mapChoisie->mapSprite.setTexture(mapChoisie->mapTexture);
        mapChoisie->animation.delayMillis = 85.0f;
        mapChoisie->animation.offsetX = 0;
        mapChoisie->mapSprite.setPosition(0,0);
        mapChoisie->mapSprite.scale((float)SCREEN_WIDTH / mapChoisie->animation.animCoord.width,(float) SCREEN_HEIGHT / mapChoisie->animation.animCoord.height);


}

Anim setAnimation(IntRect coord, int frameLimit, float delayInterFrames)
{
    Anim results;
    results.animCoord = coord;
    results.delayMillis = delayInterFrames;
    results.frameLimit = frameLimit;
    results.offsetX = coord.left;
    return results;
}

int pause(Font f, RenderWindow &fen)
{
    bool enPause = true;
    int enJeu = 1, selection = -1;
    while(enPause)
    {

        Event event;
        while(fen.pollEvent(event))
        {
            switch(event.type)
            {
            case Event::Closed:
                enPause = false;
                break;
            case Event::KeyPressed:
                if(event.key.code == Keyboard::Escape)
                    enPause = false;
                break;
            case Event::MouseMoved:
                selection = choixPause(event.mouseMove.x, event.mouseMove.y);
                break;
            case Event::MouseButtonPressed:
                if(selection != -1)
                {
                    enPause = 0;
                    if (selection == 1)
                        enJeu = 0;
                }

            default:
                break;
            }
            fen.clear();
            affichagePause(selection, f, fen);

            fen.display();

        }
    }
    return enJeu;
}

void trueAnimation(Fighter *fighterPointer, int animation, int nbAnim)
{
    Fighter theFighter = *fighterPointer;
    if(theFighter.canAnim)
    {
        //system("PAUSE");
        if(theFighter.animBegin)
        {
            theFighter.trueAnimCl.restart();
            theFighter.fighterCl.restart();
            //theFighter.controlAcces = false;
            theFighter.animBegin = false;
        }

        float dureeAnim;
        dureeAnim = theFighter.animation[animation].delayMillis*theFighter.animation[animation].frameLimit;

        if (theFighter.trueAnimCl.getElapsedTime().asMilliseconds() < dureeAnim*nbAnim)
        {
            if(theFighter.fighterCl.getElapsedTime().asMilliseconds()  > theFighter.animation[animation].delayMillis)
            {
                if(theFighter.animation[animation].animCoord.left > (theFighter.animation[animation].frameLimit-2)*(theFighter.animation[animation].animCoord.width)+theFighter.animation[animation].offsetX)
                {
                    theFighter.animation[animation].animCoord.left = theFighter.animation[animation].offsetX;
                }

                else
                    theFighter.animation[animation].animCoord.left += theFighter.animation[animation].animCoord.width;

                theFighter.fighterCl.restart();
            }
            if (theFighter.attackQualification.attackBurst == true && theFighter.attackQualification.special == true)
            {
                theFighter.stand.setTextureRect(theFighter.animation[animation].animCoord);
                theFighter.fighter.setOrigin(theFighter.animation[animation].animCoord.width/2, theFighter.animation[animation].animCoord.height);
            }
            else
            {
                theFighter.fighter.setTextureRect(theFighter.animation[animation].animCoord);
                theFighter.fighter.setOrigin(theFighter.animation[animation].animCoord.width/2, theFighter.animation[animation].animCoord.height);
            }

        }
        else
        {
            theFighter.canAnim = false;
            theFighter.animBegin = true;
            //theFighter.controlAcces = true;
        }
    }
    *fighterPointer = theFighter;
}
