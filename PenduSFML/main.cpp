#include <SFML/Graphics.hpp>
#include <string.h>
#define SYMBOLE '*'
#define ERREURS_MAX 8
#define LETTRES_MAX 27
#define ERREUR1 "dessin-jeu-pendu-1.gif"
#define ERREUR2 "dessin-jeu-pendu-2.gif"
#define ERREUR3 "dessin-jeu-pendu-3.gif"
#define ERREUR4 "dessin-jeu-pendu-4.gif"
#define ERREUR5 "dessin-jeu-pendu-5.gif"
#define ERREUR6 "dessin-jeu-pendu-6.gif"
#define ERREUR7 "dessin-jeu-pendu-7.gif"
#define ERREUR8 "dessin-jeu-pendu-8.gif"


using namespace sf ;

Font chargeFont();
void initialiseMotCrypte ( char motAdeviner[], char motCrypte[]);
int chercheEtRemplaceLettre  (char motAdeviner[], char motCrypte[ ],char  lettre );
int aGagne (char motAdeviner[], char motCrypte[]);
int main()
{
    Font font = chargeFont();


    // Create the main window
    RenderWindow fenetre1( VideoMode(800, 600), "JEU DU PENDU - JOUEUR 1");

    char motAdeviner [LETTRES_MAX];
    int i =0 ;
    char motCrypte [LETTRES_MAX];
    Text textJoueur, textMotAdeviner;
    textMotAdeviner.setFont(font);
    textMotAdeviner.setPosition(0,60);
    textJoueur.setString("JOUEUR 1 : entre un mot");
    textJoueur.setFont(font);
    textJoueur.setStyle(Text::Underlined);
    // Start the game loop
    while (fenetre1.isOpen())
    {
        // Process events
        Event event;
        while (fenetre1.pollEvent(event))
        {
            // Close window : exit
            if (event.type ==  Event::Closed)
            {
                fenetre1.close();
                exit(0);
            }
            if (event.type ==  Event::TextEntered)
            {
                printf("Touche pressee : %c\n",event.text.unicode);
                char carac = event.text.unicode;
                if ( isalpha(carac))
                {
                    motAdeviner[i]=carac;
                    i++;
                    motAdeviner[i]='\0';
                    textMotAdeviner.setString(motAdeviner);
                }
            }
            if (event.type == Event::KeyPressed)
            {
                if ( event.key.code == Keyboard::Return)
                {

                    printf("Mot : %s",motAdeviner);
                    fenetre1.close();
                }
            }

        }

        // Clear screen
        fenetre1.clear();
        fenetre1.draw(textJoueur);
        fenetre1.draw(textMotAdeviner);
        fenetre1.display();

    }

    Sprite imagePendu;
    imagePendu.setPosition(0,100);
    Texture pendu ;
    RenderWindow fenetre2( VideoMode(800, 600), "JEU DU PENDU - JOUEUR 2");
    textJoueur.setString("JOUEUR 2 : Entre une lettre ");
    initialiseMotCrypte (  motAdeviner,  motCrypte);
    Text textMotCrypte ;
    textMotCrypte.setFont(font);
    textMotCrypte.setPosition(0,60);
    textMotCrypte.setString(motCrypte);
    int nbErreur = 0;

    while (fenetre2.isOpen())
    {
        // Process events
        Event event;
        while (fenetre2.pollEvent(event))
        {
            // Close window : exit
            if (event.type ==  Event::Closed)
            {
                fenetre2.close();
                exit(0);
            }
            if (event.type ==  Event::TextEntered)
            {
                printf("Touche pressee : %c\n",event.text.unicode);
                char carac = event.text.unicode;
                if ( isalpha(carac))
                {
                    int nbFois = chercheEtRemplaceLettre  ( motAdeviner,  motCrypte,carac ) ;
                    textMotCrypte.setString(motCrypte);
                    if ( nbFois == 0)
                    {
                       char nomFichier[40];
                       nbErreur ++;
                       sprintf( nomFichier,"images\\dessin-jeu-pendu-%i.gif",nbErreur);
                        bool chargeOK = pendu.loadFromFile(nomFichier) ;
                        if (! chargeOK)
                            printf("PB de chargement de l'image pendu!\n");
                        imagePendu.setTexture(pendu);

                    }
                }
            }
        }
        fenetre2.clear();
        fenetre2.draw(textJoueur);
        fenetre2.draw(textMotCrypte);
        fenetre2.draw(imagePendu);

        fenetre2.display();
    }

    return EXIT_SUCCESS;
}

Font chargeFont()
{
    Font f ;
    if (!f.loadFromFile("c:\\windows\\fonts\\arial.ttf"))
    {
        printf("pb chargement police");
        exit(1);

    }
    return f;
}


void initialiseMotCrypte ( char motAdeviner[], char motCrypte[])
{
    int i =0 ;
    while ( motAdeviner[i] != '\0')
    {
        motCrypte[i] = SYMBOLE;
        i ++ ;
    }

    motCrypte[i]='\0';
}

int chercheEtRemplaceLettre  (char motAdeviner[], char motCrypte[ ],char  lettre )
{
    int res =0;
    int nb=strlen(motAdeviner);
    int i;
    for(i=0; i<nb; i++)
    {
        if ( motAdeviner[i] == lettre )
        {
            motCrypte[i] = lettre ;
            res ++ ;
        }
    }

    return res ;
}

int aGagne (char motAdeviner[], char motCrypte[])
{
    int gagne ;
    if ( strcmp( motAdeviner, motCrypte ) == 0 )
        gagne = 1 ;
    else
        gagne = 0;
    return gagne ;
}
