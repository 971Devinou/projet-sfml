#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <time.h>
#include "pendu.hpp"
#define SYMBOLE '*'
#define ERREURS_MAX 8
#define LETTRES_MAX 27

void initialiseMotCrypte (char motAdeviner[], char motCrypte[])
{
    int i = 0;
    while(motAdeviner[i] != '\0')
    {
        motCrypte[i] = SYMBOLE;
        i++;
    }
    motCrypte[i] = '\0';
}

int chercheEtRemplaceLettre (char *motAdeviner, char *motCrypte, char lettre)
{
    int i = 0;
    int result = 0;
    while(motAdeviner[i] != '\0')
    {
        if(motAdeviner[i] == lettre)
        {
            motCrypte[i] = lettre;
            result++;
        }
        i++;
    }
    return result;
}

int aGagne (char *motAdeviner, char *motCrypte)
{
    int i = 0;
    int result = 1;
    do
    {
        if(motAdeviner[i] != motCrypte[i])
            result = 0;
        i++;
    }while(motAdeviner[i] != '\0');
    return result;
}

void afficheBravoMC ( char lettre )
{
    printf ("----------------------------------------------------\n");
    printf ("Bravo ! %c est bien dans le mot a trouver.\n",lettre);
    printf ("----------------------------------------------------\n");
}

void afficheDommageMC ( char lettre,int nbErr )
{
    printf ("----------------------------------------------------\n");
    printf ("Dommage ! %c n'est pas dans le mot a trouver.\n",lettre);
    printf ("vous avez fait %i erreur(s) / %i .\n",nbErr, ERREURS_MAX);
    printf ("----------------------------------------------------\n");
}

char saisieRejouerMC()
{
    printf ("----------------------------------------------------\n");
    printf("Rejouer (O/N) ?\n");
    char choix;
    fflush(stdin);
    scanf("%c",&choix);
    choix = toupper(choix);
    while ( choix!= 'O' && choix!= 'N' )
    {
        printf ("Erreur, vous avez le choix entre O et N :\n");
        fflush(stdin);
        scanf("%c",&choix);
        choix = toupper(choix);
    }
    return choix;
}

char saisieLettreMC ()
{
    char lettre;
    fflush(stdin);
    scanf("%c",&lettre);
    while ( isalpha(lettre) ==0)
    {
        printf("Attention, vous devez saisir une lettre :\n");
        fflush(stdin);
        scanf("%c",&lettre);
    }
    return lettre;
}

char joueMC ( int nbEssais)
{
    printf ("\nEssai : %i\n",nbEssais);
    printf ("\nDonne une lettre \n");
    return saisieLettreMC();
}
void afficheEnteteJoueurMC ( int num)
{
    system("cls");
    printf ("\n----------------------------------------------------\n");
    printf ("\nJoueur %i .... \n",num);
    printf ("----------------------------------------------------\n");
}

void afficheMsgFinJeuMC(int gagne)
{
    system("cls");
    printf ("----------------------------------------------------\n");
    if (gagne)
        printf("BRAVO, c'est gagne ! Le mot etait bien :\n");
    else
        printf("DOMMAGE, c'est perdu ! Le mot etait :\n");
    printf ("----------------------------------------------------\n");
}

void saisieMotAdevinerMC(char motAdeviner[ ])
{
    printf("Entre le mot que doit deviner l'autre joueur :\n");
    scanf("%s",motAdeviner);
}

void afficheMotMC(char mot[ ])
{
    printf ("%s", mot);
}

