#include <SFML/Graphics.hpp>

void initialiseMotCrypte (char motAdeviner[], char motCrypte[]);
int chercheEtRemplaceLettre (char *motAdeviner, char *motCrypte, char lettre);
int aGagne (char *motAdeviner, char *motCrypte);
void afficheEnteteJoueurMC ( int num);
void afficheMsgFinJeuMC (int gagne );
void afficheBravoMC ( char lettre ) ;
void afficheDommageMC ( char lettre,int nbEssais);
char saisieLettreMC ();
char saisieRejouerMC();
void saisieMotAdevinerMC( char motAdeviner [ ]) ;
void afficheMotMC(char mot[ ]);
char joueMC ( int nbEssais);

