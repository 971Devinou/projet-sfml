#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>

using namespace sf;

#define JOTARO_SPRITESHEET "img/jotaro_spritesheet_correct.png"
#define FRAMERATE 60
#define WINDOW_SIZE 900
#define PLAYER_IDLE_X 2598
#define PLAYER_IDLE_Y 3161
#define IDLE_X 6
#define IDLE_Y 34
#define CHARAC_SIZE_IDLE_X 37
#define CHARAC_SIZE_IDLE_Y 57

int pasCharac(RenderWindow* app)
{
    Event event;
    int pas;

    while (app->pollEvent(event))
        {
        switch (event.type)
        {
        case Event::KeyPressed:
            if(event.key.code == Keyboard::D)
            {
                pas = 1;
            }
            if(event.key.code == Keyboard::Q)
            {
                pas = -1;
            }
            break;
        default:
            pas = 0;
            break;
        }
         return pas;
}
}

void basicAnim(RenderWindow* app)
{
    int i, pas;
    pas = pasCharac(&(*app));
    Texture anim;
    Sprite jotaro;

    anim.loadFromFile(JOTARO_SPRITESHEET);
    jotaro.setTexture(anim);


    jotaro.setScale(3,3);
    //float temps;
    while (app->isOpen())
    {
    //Time elapsed;

    Event event;
        while (app->pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app->close();
        }
    for (i=0; i<6; i++)
    {

        //if ( >= milliseconds(100))
        while(pas = 1)
        {
            pas = pasCharac(&(*app));
            jotaro.setPosition(jotaro.getPosition().x+pas, jotaro.getPosition().y);
        }
        app->clear();
        jotaro.setTextureRect(IntRect(IDLE_X+CHARAC_SIZE_IDLE_X*i,IDLE_Y, CHARAC_SIZE_IDLE_X ,CHARAC_SIZE_IDLE_Y));
        app->draw(jotaro);
        app->display();
        sleep(milliseconds(100));
        }
        }

    }

int main()
{
    RenderWindow app(VideoMode(WINDOW_SIZE,WINDOW_SIZE),"JOTARO !?");
    app.setFramerateLimit(FRAMERATE);

    bool theWorld = false;

    do
    {
        app.display();
        printf("Initialisation....\n");
    }
    while (!app.isOpen());

    // Start the game loop
    while (app.isOpen())
    {
        // Process events
        Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
        }
        //app.clear();
        // Draw the sprite
        //app.draw();
        // Update the window
        //app.display();
        basicAnim(&app);
    }

    return 0;
}
