//Fait grace � un programme python
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string.h>
#include "test.hpp"
#define JOTARO "jotaro_spritesheet_correct.png"
#define SCREEN_WIDTH 1300
#define SCREEN_WIDTH 700
#define FPS 120
using namespace sf;

int main()
{
    int frame = 0;
    RenderWindow fen(VideoMode(800, 600), "Jojo Star Fighter");
    fen.setFramerateLimit(FPS);
    Personnage jotaro;
    //jotaro = creerPersonnage(0, "Jotaro", "Star Platinium", JOTARO);
    //jotaro.nom = "Jotaro";
    //jotaro.nomStand = "Star Platinium";
    jotaro.id = 0;
    jotaro.persoTex.loadFromFile(JOTARO);
    jotaro.animPerso[0].offsetX = 6;
    jotaro.animPerso[0].offsetY = 34;
    jotaro.animPerso[0].sizeX = 37;
    jotaro.animPerso[0].sizeY = 57;

    while (fen.isOpen())
    {

        Event event;
        while (fen.pollEvent(event))
        {
            if (event.type == Event::Closed)
                fen.close();
            animations(&jotaro, 0, &frame);
        }

        fen.clear();
        fen.draw(jotaro.persoSprite);
        fen.display();
    }

    return EXIT_SUCCESS;
}

