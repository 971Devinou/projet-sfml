#include <SFML/Graphics.hpp>
#include <string.h>

using namespace sf;

typedef struct
{
    int offsetX, offsetY, sizeX, sizeY;
    int maxFrames;
} AnimCoord;

typedef struct
{
    int id;
    String nom[100];
    String nomStand[100];
    const int ptsVie = 250;
    const int special = 100;
    const int buff = 15;
    AnimCoord animPerso[];
    Texture persoTex;
    Sprite persoSprite;
} Personnage;

int prendDegats(int ptsVie, int degats);
Personnage creerPersonnage(int id, String nom, String nomStand, String spriteSheet);
void animations(Personnage* instance, int anim, int* frame);
