#include "test.hpp"
#include <SFML/Graphics.hpp>
#include <string.h>

using namespace sf;

int prendDegats(int ptsVie, int degats)
{
    ptsVie = ptsVie - degats;
    return ptsVie;
}

Personnage creerPersonnage(int id, String nom, String nomStand, String spriteSheet)
{
    Personnage instance;
    instance.id = id;
    //strcpy(nom, instance.nom);
    //instance.nom = nom;
    //strcpy(nomStand, instance.nomStand);
   // instance.nomStand = nomStand;
    instance.persoTex.loadFromFile(spriteSheet);
    return instance;
}

void animations(Personnage* instance, int anim, int* frame)
{
    if (*frame > instance->animPerso[anim].maxFrames)
    {
        *frame = 0;
    }
    instance->persoSprite.setTextureRect(IntRect(instance->animPerso[anim].offsetX + (instance->animPerso[anim].offsetX)*(*frame),instance->animPerso[anim].offsetY,instance->animPerso[anim].sizeX,instance->animPerso[anim].sizeY));
    instance->persoSprite.setScale(instance->animPerso[anim].sizeX/instance->persoSprite.getScale().x,instance->animPerso[anim].sizeY/instance->persoSprite.getScale().y);
    *frame++;
}
