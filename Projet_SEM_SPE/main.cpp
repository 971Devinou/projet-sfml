//Fait grace � un programme python
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string.h>
#include "menu.h"
#define SCREEN_WIDTH 1300
#define SCREEN_LENGHT 700
#define FPS 120
#define JOTARO_SHEET "jotaro_spritesheet_correct.png"
using namespace sf;

int main()
{

    RenderWindow fen(VideoMode(SCREEN_WIDTH, SCREEN_LENGHT), "Jojo Star Fighter");
    fen.setFramerateLimit(FPS);

    while (fen.isOpen())
    {
        Event event;
        while (fen.pollEvent(event))
        {
            if (event.type == Event::Closed)
                fen.close();


        }
        fen.display();

    }

    return EXIT_SUCCESS;
}

