#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


using namespace sf;

typedef struct
{
    int offsetX;
    IntRect animCoord;
    int frameLimit;
    float delayMillis;
    //SoundBuffer animBuffer;
} Anim;

typedef struct
{
    bool attack = false;
    bool attackStand = false;
} AttackType;

typedef struct
{
    bool externStand;
    char nom[20];
    int vie = 500;
    int VIE_MAX = 500;
	bool jump = false;
    int fighterAnim = 0;
    int standAnim = 0;
    Sprite fighter;
    Sprite stand;
    Clock fighterCl;
    Clock standCl;
    bool guardActive = 0;
    bool standActive = 0;
    Vector2f velocity = Vector2f(0.f,0.f);
    int standOffset = 0;
    Texture fighterText;
    Sprite spriteSheet;
    Anim animation[15];
    AttackType attackMode;
    int fighterFrame = 0;
    //Sound fighterSound;
} Fighter;

typedef struct
{
    Sprite mapSprite;
    Clock montreSprite;
    int standOffset = 0;
    Texture mapTexture;
    Anim animation;
} Map;


