#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <SFML/Graphics.hpp>
#include "AffichePDV.hpp"

#define NOM_ARENE1 "Arene1.jpg"
#define NOM_ARENE2 "Arene2.PNG"
#define NOM_ARENE3 "Arene3.PNG"
#define NOM_ARENE4 "Arene4.PNG"
#define NOM_ARENE5 "Arene5.PNG"




void choixArene(int valeurChoixArene)
{
    if(valeurChoixArene==1)
    {
         Texture image;
        Sprite ARENE1;
        image.loadFromFile(NOM_ARENE1);

        ARENE1.setTexture(image);
        ARENE1.setPosition(0,0);
        ARENE1.setScale((2),(2.1));
        app.draw(ARENE1);
        app.display();

    }

    if (valeurChoixArene==2)
    {

        RenderWindow app(VideoMode(1300,700),"Test");

        Texture image;
        Sprite ARENE2;
        image.loadFromFile(NOM_ARENE2);


        ARENE2.setTexture(image);
        ARENE2.setPosition(0,0);
        ARENE2.setScale((1.7),(2.1));
        app.draw(ARENE2);
        app.display();


    }

    if(valeurChoixArene==3)
    {
        Texture image;
        Sprite ARENE3;
        image.loadFromFile(NOM_ARENE3);

         ARENE3.setTexture(image);
        ARENE3.setPosition(0,0);
        ARENE3.setScale((1.8),(3.1));
        app.draw(ARENE3);
        app.display();

    }

    if(valeurChoixArene==4)
    {
              Texture image;
        Sprite ARENE4;
        image.loadFromFile(NOM_ARENE4);

         ARENE4.setTexture(image);
        ARENE4.setPosition(0,0);
        ARENE4.setScale((1.7),(2.1));
        app.draw(ARENE4);
        app.display();

    }

    if(valeurChoixArene==5)
    {
        Texture image;
        Sprite ARENE5;
        image.loadFromFile(NOM_ARENE5);

         ARENE5.setTexture(image);
        ARENE5.setPosition(0,0);
        ARENE5.setScale((2.1),(1.9));
        app.draw(ARENE5);
        app.display();

    }
}
